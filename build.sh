#!/bin/sh

LIBRARY=nanolab-1.0.jar

# compile java files
cd src
find . -name "*.java" > sources.txt
javac -Xlint:unchecked -encoding UTF8 @sources.txt
rm sources.txt

# package in a library
mkdir -p ../bin
jar cvf ../bin/$LIBRARY $(find . -name *.class) > /dev/null 2>&1

# delete all class files
find . -name "*.class" -type f -delete


Nanolab Java implementation
================

## Introduction
Nanolab is a high level numerical computing scripting language.  
It allows scalar, list, matrix, string manipulation and implementation of algorithms.

The Nanolab Java implementation allows easy integration with any Java applications.

## About
Nanolab was initially designed to bring an easy to use scripting language for Java games.  
It naturally evolved into a robust fully documented language.

## Documentation
The [language reference](http://galmiza.net/public/nanolab/Nanolab_Language_reference.pdf) introduces the language and its features.  
The [function reference](http://galmiza.net/public/nanolab/Nanolab_Function_reference.pdf) introduces the built-in functions that come with the implementation.


# Test in a command line terminal
Clone the repo in your local drive
```
#!shell
git clone https://github.com/galmiza/nanolab-java
```
Jump into the nanolab-java folder
```
#!shell
cd nanolab-java
```
Compile nanolab sources and generate jar
```
#!shell
./build.sh
```
Compile test sources and run the self test or a live interpreter
```
#!shell
./test.sh
```


# Developer guide
Nanolab for Java comes as a standard Java package.  
It uses the [Jama](http://math.nist.gov/javanumerics/jama/) library for matrix operations.

Copy these packages into your project and you are done!

## Start the parser
You first need to create a Parser object.

```
#!java
// Init the parser
Parser parser = new Parser();
```

Then optionally load the default operators (*,^,/,&&,...), variables (Pi, true, ...) and functions.
```
#!java
// Load default configuration
Init.loadOperators(parser);
Init.loadVariables(parser);
Init.loadNativeFunctions(parser);
Init.loadScriptedFunctions(parser);
```

You are ready to go!
```
#!java
parser.runExpression("m = {{1,3,5},{2,4,6}}");
parser.runExpression("m2 = transpose(m)");
parser.runExpression("value = m2[2][1]*sin(Pi/3!)");
```

## Access the variables
The Node class lets you interact with the variables.
```
#!java
Node node = parser.expandNode("value");
System.out.println(node.getDoubleValue());
```


## Integrate custom Java functions
You can define your own operators, variables and functions.

Below is a full example of defining the "split" function and adding it to the parser.
```
#!java
static Pointer split() { // split(separator, string)
	return new Pointer() {
		@Override
		public Node run(Node node) throws Exception {
			if (node.getChildrenCount()==2) {
				Node sep = parser.runNode(node.getChild(0));
				Node str = parser.runNode(node.getChild(1));
				
				if (sep.isString() && str.isString()) {
					node.setList();
					for (String s : str.getString().split(sep.getString()))
						node.addChild(new Node(s));
					return node;
				} else
					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
			} else
				throw new Exception(parser.getArgumentCountExceptionString(this, 2));
		}
	};
}
```
```
#!java
parser.addFunction("split", split(), Function.TYPE_BUILT_IN);
```


# Android
You can download an implementation on Android on [Google Play](https://play.google.com/store/apps/details?id=net.galmiza.android.nanolab).  
The application lets you use Nanolab from a command line screen and lets you draw nice graphics!
import java.util.Scanner;

import net.galmiza.nanolab.engine.Init;
import net.galmiza.nanolab.engine._LibTools;
import net.galmiza.nanolab.engine.Parser;


public class Interpreter {

	static Parser parser;
	public static void main(String[] args) {
		
		// Parser
		try {
			parser = new Parser();
			_LibTools.parser = parser;
			
			// Load configuration
			Init.loadOperators(parser);
			Init.loadVariables(parser);
			Init.loadNativeFunctions(parser);
			Init.loadScriptedFunctions(parser);
			
			// Get user input until "exit" is typed
			Scanner in = new Scanner(System.in);
			do {
			    String query = in.nextLine();
			    if (query.equals("exit")) {
			    	in.close();
			    	break;
			    }
			    try {
			    	System.out.println("> "+parser.runExpression(query));
				} catch (Exception e) {  e.printStackTrace(); }
			    
			} while (true);
			in.close();
			
		} catch (Exception e) {		e.printStackTrace();	}
	}
}

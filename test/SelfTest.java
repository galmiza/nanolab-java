import java.util.Scanner;

import net.galmiza.nanolab.engine.Init;
import net.galmiza.nanolab.engine._LibTools;
import net.galmiza.nanolab.engine.Parser;


public class SelfTest {

	static Parser parser;
	private static int failedCount = 0;
	private static int passedCount = 0;
	private static int skippedCount = 0;

	// Entry point
	public static void main(String[] args) {
		
		// Parser
		try {
			parser = new Parser();
			_LibTools.parser = parser;
			
			// Load configuration
			Init.loadOperators(parser);
			Init.loadVariables(parser);
			Init.loadNativeFunctions(parser);
			Init.loadScriptedFunctions(parser);
			
			// Run test cases
			run();
			
		} catch (Exception e) {		e.printStackTrace();	}
	}

	private static void run() {
		
		// String concat
		run("\"hello\"+\"world\"", "\"helloworld\"");
		
		// Scalar sums
		run("3+6", "9");
		run("3+6.043", "9.043");
		
		// Types
		run("isFloat(0.4)","true");
		run("isInteger(11)","true");
		run("isString(\"helloworld\")","true");
		run("isVariable(i)","true");
		run("isList({1,2})","true");
		run("isBoolean(true)","true");
		
		run("isFloat(4)","false");
		run("isInteger(\"hello\")","false");
		run("isString({\"list\"})","false");
		run("isVariable({})","false");
		run("isList(i)","false");
		run("isBoolean(0)","false");		
		
		// 0/1 float
		run("0.0","0");
		run("1.0","1");
		
		// Arrays
		run("{1,2.3,\"4&(_%£}\",{1,3,5}}", "");
		run("{-5}", "{-5}");
		run("{5}", "{5}");
		run("{-5}*{-5}", "{25}");
		run("{-5}*{5}", "{-25}");
		run("{5}*{5}", "{25}");
		run("{{-5}*{-5}}", "{{25}}");
		run("{{-5}*{5}}", "{{-25}}");
		run("{{5}*{5}}", "{{25}}");
		
		
		// Scalar + vector sums
		run("3+{5,6,7.7}", "{8,9,10.7}");
		run("3.53+{5,6,7.7}", "{8.53,9.53,11.23}");
		run("{5,6,7.7}+3", "{8,9,10.7}");
		run("{5,6,7.7}+3.53", "{8.53,9.53,11.23}");
		
		// Vector + vector sums
		run("{1,2.3,4}+{5,6,7.7}", "{6,8.3,11.7}");
		run("{1,2.3,{4}}+{5,6,{7.7}}", "{6,8.3,{11.7}}");
		run("{1,{2.3,{4}}}+{5,{6,{7.7}}}", "{6,{8.3,{11.7}}}");
		run("{{1,2,3}+{4,5,6}+{7,8,9}}", "{{12,15,18}}");
		
		// Scalar / matrix
		run("1.4+{{1,2,3},{4,5,6},{7,8,9}}", "{{2.4,3.4,4.4},{5.4,6.4,7.4},{8.4,9.4,10.4}}");
		run("1+{{1,2,3},{4,5,6},{7,8,9}}", "{{2,3,4},{5,6,7},{8,9,10}}");
				
		// Trigonometry
		run("sin(0)", "0");
		run("sin(1.0)", "0.841471");
		run("sin({1,2})", "{0.841471,0.909297}");
		run("sin({0.9,0.4,{3.4,{0,5,4.3},0},5.6})", "{0.783327,0.389418,{-0.255541,{0,-0.958924,-0.916166},0},-0.631267}");
		
		// JSON-like
		run("{\"name\":\"adam\",\"age\":29,\"sex\":\"male\"}", "{{\"name\",\"adam\"},{\"age\",29},{\"sex\",\"male\"}}");
		
		// Variables
		run("a=1", "1");
		run("a=a+1", "2");
		run("a", "2");
		run("a=2", "2");
		run("a=a+a+3", "7");
		run("a", "7");
		run("a++","8");
		run("a","8");
		run("a--","7");
		run("a","7");
		run("a+=3","10");
		run("a","10");
		run("a-=6","4");
		run("a","4");
		run("a*=3","12");
		run("a","12");
		run("a/=4","3");
		run("a","3");
		run("unset(a)", "a");
		run("a", "a");
		run("b=c", "c");
		run("c=d", "d");
		run("b", "d");
		run("i=j", "j");
		run("j=1", "1");
		run("i", "1");
		run("j=2", "2");
		run("i", "2");
		run("unset(i)", "SKIP"); // because i is expected to be unset on further tests
		run("unset(p)","SKIP");
		
		// Reverse operator
		run("{k+8*p,8*p+k,k*(8+p)}","{k+8*p,8*p+k,k*(8+p)}");
		
		// Children
		run("getChild(1,{5,6,7})", "6");
		run("a={7.6,\"hi\",\"yop\"}", "{7.6,\"hi\",\"yop\"}");
		run("getChild(2,a)", "\"yop\"");
		run("a[0]", "7.6");
		run("b=1", "1");
		run("a[b]", "\"hi\"");
		run("a[1+1]", "\"yop\"");
		run("a={1,2,3,{4,{5,6},7,8}}", "{1,2,3,{4,{5,6},7,8}}");
		run("a[3]", "{4,{5,6},7,8}");
		run("a[3][1]", "{5,6}");
		run("a[3][1][0]", "5");
		run("a[{3,1,0}]", "5");
		run("a[a[1]+1][int(\"1\")][int(sin(0))]", "5");
		run("a[2]=9", "{1,2,9,{4,{5,6},7,8}}");
		run("a[3][1][1]=0", "{1,2,9,{4,{5,0},7,8}}");
		run("setChild({3,1,1},a,0)","{1,2,9,{4,{5,0},7,8}}");
		run("a={1,2,3}","SKIP");
		run("addChild(a,7)","{1,2,3,7}");
		run("rmChild(1,a)","{1,3,7}");
		run("insChild(2,a,\"hi\")","{1,3,\"hi\",7}");
		
		// Misc
		run("1+{2,3,i,4,5}","{3,4,1+i,5,6}");
		run("2*i-7-9*(i-7)","-7*i+56");
		run("t=range(0.0,2*π,0.03)", "SKIP");
		
		// Dot product
		run("dot({2,3,4,5},{1,3,5,7})","66");

		// Cross product
		run("cross({2,3,4},{5,6,7})","{-3,6,-3}");
		run("cross(p,k)","{p[1]*k[2]-p[2]*k[1],p[2]*k[0]-p[0]*k[2],p[0]*k[1]-p[1]*k[0]}");
		
		// Transpose vector
		run("transpose({{1,3,5,7}})","{{1},{3},{5},{7}}");
		run("transpose({{1},{3},{5},{7}})","{{1,3,5,7}}");
		
		// Matrix
		run("zeros(4,3)","{{0,0,0},{0,0,0},{0,0,0},{0,0,0}}");
		run("ones(4,3)","{{1,1,1},{1,1,1},{1,1,1},{1,1,1}}");
		run("eye(3)","{{1,0,0},{0,1,0},{0,0,1}}");
		run("size({{1,3,5},{2,4,6}})","{2,3}");
		run("size({{1,3},{2,4}})","{2,2}");
		run("isSquare({{1,0,0},{0,1,0},{0,0,1}})","true");
		run("isSquare({{1,0,0},{0,1,0},{0,0,1},{0,0,1}})","false");
		run("transpose({{1,3,5},{2,4,6}})","{{1,2},{3,4},{5,6}}");
		run("transpose({{1,2},{3,4},{5,6}})","{{1,3,5},{2,4,6}}");
		run("inv({{2,3},{4,5}})","{{-2.5,1.5},{2,-1}}");
		run("det({{2,3},{4,5}})","-2");
		run("trace({{2,3},{4,5}})","7");
		run("{{1,2,3},{4,5,6},{7,8,9}}+{{4,5,6},{7,8,9},{1,2,3}}", "{{5,7,9},{11,13,15},{8,10,12}}");
		run("{{1,2,3},{4,5,6}}*{{1,2},{3,4},{5,6}}","{{22,28},{49,64}}");
		run("{{2,3},{4,6}}*{{1,2},{5,6}}","{{17,22},{34,44}}");
		
		// Solve A*x=B
		
		// Norm
		run("norm2({1,-2,3})","14");
		
		// Strings
		run("length(\"helloworld\")","10");
		run("join(\"_\",{\"a\",\"b\",\"c\"})","\"a_b_c\"");
		run("charAt(4,\"abcdefg\")","\"e\"");
		run("substring(2,3,\"abcdefg\")","\"cde\"");
		run("replace(\"def\",\"xyztu\",\"abcdefg\")","\"abcxyztug\"");
		run("indexOf(\"45\",\"12345678\")","3");
		run("split(\"-\",\"R-g-54-RTC\")","{\"R\",\"g\",\"54\",\"RTC\"}");
		
		// Aggregation
		run("sum({2,3,4,5})","14");
		run("count({2,3,5,6})","4");
		run("avg({2,4,4,4,5,5,7,9})","5");
		run("count({2,4,4,4,5,5,7,9})","8");
		run("std({2,4,4,4,5,5,7,9})","2");
		
		// Time
		//run("time()","1386979533");
		//run("time(\"yyyy-MM-dd\")","\"2013-12-14\"");
		
		// File system
		//run("ls()","{}");
		
		// Base conversion
		run("decToBin(5)","\"101\"");
		run("decToHex(255)","\"FF\"");
		run("decToHex(100)","\"64\"");
		run("binToDec(\"11011\")","27");
		
		// Function definition
		run("f(a,b)=a*(2+b)","SKIP");
		run("f(3,5)","21");
		
		// Algorithm
		run("{u=5,u=4*u,u=31}", "{5,20,31}");
		run("u", "31");
		run("fact1(n)={r=1,for(i,2,n,{r=r*i}),return(r)}","SKIP");
		run("fact1(6)","720");
		run("fact1(6)","720"); // let is run another time

		// If
		run("testif1(a,b)={r=if(a,if(b,\"11\",\"10\"),if(b,\"01\",\"00\")),return(r)}","SKIP");
		run("testif1(0,11)","\"01\"");
		run("testif1(0,0)","\"00\"");
		run("testif1(9,2)","\"11\"");
		run("testif2(a)=if(a,i=1,i=0)","SKIP");
		
		// For
		run("testfor1(u,v)=for(i,0,u,for(j,0,v,print(string(i)+string(j))))","SKIP");
		run("b={0,0,0,0,0}","SKIP");
		run("testfor2(u,v)={for(i,0,u,for(j,0,v,b[i+j]=b[i+j]+1)),return(b)}","SKIP");
		run("testfor2(2,2)","{1,2,3,2,1}");
		run("testfor3(a,b)=for(i,a,b,{print(\"Graph \"+string(i)),plot(exp(cos(i*t)),sin(1+3*i*t))})","SKIP");
		run("testfor5()={a={1,2,3},for(i,a,i+=1),return(a)}","SKIP");
		run("testfor5()","{2,3,4}");
		
		// While
		run("testwhile1(n)={r=0,while(n>3,{n=n-1,r+=n}),return(r)}","SKIP");
		run("testwhile1(10)","42"); //9+8+7+6+5+4+3
		
		// Return
		run("for(i,0,10,{if(i>6,return(i),0),print(i)})","SKIP");
		run("i","7");
		run("testfor4(a,b,c)=for(i,0,a,for(j,0,b,if(j>=c,return(j),0)))","SKIP"); // nested loops
		run("testfor4(10,10,4)","4");
			
		// Recursive
		run("fact2(n)=if(int(n-1),n*fact2(n-1),n)","SKIP");
		run("fact2(6)","720");
		run("fact2(6)","720"); // let is run another time
		
		// Break
		run("breaktest()={r={},m={{1,2,3,4},{5,6,7,8},{9,10,11,12}},for(i,0,2,for(j,0,3,if(j==2,break(),addChild(r,m[i][j])))),return(r)}","SKIP");
		run("breaktest()","{1,2,5,6,9,10}");
		
		// Complex runtime assignments
		run("b(v)={a(u)=2*u,return(a(v))}","SKIP");
		run("b(5)","10");
		
		// Complex algorithms
		run("{t(a,b)={r=0,for(i,0,a,if(int(i-b/2),r=r+1,r=r*2)),return(r)},t(11,7)}","SKIP");
		run("t(11,7)","19");
		
		// Variable scope
		run("r=10","10");
		run("t(11,7)","19"); // r is manipulated in t function
		run("r","10");
		run("t(i,j,k)={v[i][j]=k}","SKIP");
		run("t(a,b,c)","v[{a,b}]=c");
		
		// Stats
		System.out.println("Total: "+(passedCount+failedCount+skippedCount));
		System.out.println("Total passed: "+passedCount);
		System.out.println("Total failed: "+failedCount);
		System.out.println("Total skipped: "+skippedCount);
		System.out.println("Success rate: "+((1f*passedCount)/(passedCount+failedCount)));
	}
	
	// Test a single expression and update test stats
	private static void run(String input, String expected) {
		String result = null;
		try {
			System.out.println("Testing: "+input);
			
			input = parser.processExpression(input);
			result = parser.runExpression(input);
			
			if (expected.equals("")) expected = input;
			if (expected.equals("SKIP")) {
				skippedCount++;
			} else if (!result.equals(expected)) {
				throw new Exception();
			} else {
				passedCount++;
			}
			
		} catch (Exception e) {
			failedCount++;
			System.err.println("FAILED: "+String.format("[%s] gave [%s] expected [%s]", input, result, expected));
		}
	}
}

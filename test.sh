#!/bin/sh

LIBRARY=nanolab-1.0.jar

# compile and run the test files
cd test
javac -encoding UTF8 -cp ../bin/$LIBRARY SelfTest.java Interpreter.java

# let user choose to run interpreter or self tests
read -p "Run Interpreter (1) or SelfTest (2) ? " ans
case $ans in
 [1]* ) java -cp ".:../bin/$LIBRARY" Interpreter; break;;
 [2]* ) java -cp ".:../bin/$LIBRARY" SelfTest; exit;;
esac

# delete all class files
find . -name "*.class" -type f -delete
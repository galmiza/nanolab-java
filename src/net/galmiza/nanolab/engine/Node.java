package net.galmiza.nanolab.engine;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import net.galmiza.nanolab.engine.Function.Pointer;



/**
 * Node is the key class of the project
 * Any expression is described as a hierarchy of nodes
 */
public class Node implements Serializable {

	// Version
	private static final long serialVersionUID = 1L;
	
	// Constants
	public static final int NODE_TYPE_BOOLEAN = 0;
	public static final int NODE_TYPE_INTEGER = 1;
	public static final int NODE_TYPE_FLOAT = 2;
	public static final int NODE_TYPE_STRING = 3;
	public static final int NODE_TYPE_LIST = 4;
	public static final int NODE_TYPE_VARIABLE = 5;
	public static final int NODE_TYPE_POINTER = 6;
	public static final int NODE_TYPE_VOID = 7;
	
	// References
	static Parser parser;
	
	// Attributes
	static boolean alive = true;
	private int type;
	private Node parent;
	private Boolean bValue;
	private BigDecimal fValue;
	private BigInteger iValue;
	private String sValue;
	transient private Pointer pointer; // used for type pointer only, transient because cannot be serialized
	private List<Node> children; // used if node is a LIST or a POINTER (WARNING, POINTERS need to call setList first to init children)
	
	// Constructors
	public Node() 				{	setVoid();	}
	public Node(Boolean b)		{	setBoolean(b);	}
	public Node(BigInteger i)	{	setInteger(i);	}
	public Node(BigDecimal f) 	{	setFloat(f);	}
	public Node(Integer i)		{	setInteger(i);	}
	public Node(Double f) 		{	setFloat(f);	}
	public Node(String s) 		{	setString(s);	}
	 
	// Getter
	public List<Node>	getChildren() 		{	return children;	}
	public Node			getChild(int i) 	{	return children.get(i);	}
	public int			getChildrenCount() 	{	return children.size();	}
	public Node			getParent()			{	return parent;	}
	public Boolean		getBoolean()		{	return bValue;	}
	public BigInteger	getInteger()		{	return iValue;	}
	public BigDecimal	getFloat()			{	return fValue;	}
	public String		getString()			{	return sValue;	}
	public Pointer		getPointer()		{	return pointer;	}
	
	// Arrays
	public float[] getFloatArray() throws Exception	{
		float[] a = new float[children.size()];
		for (int i=0; i<children.size(); i++)
			a[i] = (float) children.get(i).getDoubleValue();
		return a;
	}
	public int[] getIntegerArray() throws Exception {
		int[] a = new int[children.size()];
		for (int i=0; i<children.size(); i++)
			a[i] = (int) children.get(i).getIntValue();
		return a;
	}
	
	// Key-value getters
	public Node getNode(String key) throws Exception {
		for (Node n : children)
			if (n.isList())
				if (n.children.size()==2)
					if (n.children.get(0).isString())
						if (n.children.get(0).getString().equals(key))
							return n.children.get(1);
		throw new Exception(String.format(Error.EXECUTION_UNKNOWN_KEY, key));
	}
	public Boolean		getBoolean(String key)		throws Exception	{	return getNode(key).bValue;	}
	public BigInteger	getInteger(String key)		throws Exception	{	return getNode(key).iValue;	}
	public BigDecimal	getFloat(String key)		throws Exception	{	return getNode(key).fValue;	}
	public double		getDoubleValue(String key)	throws Exception	{	return getNode(key).getDoubleValue();	}
	public String		getString(String key)		throws Exception	{	return getNode(key).sValue;	}
	public float[]		getFloatArray(String key)	throws Exception	{	return getNode(key).getFloatArray();	}
	public int[]		getIntegerArray(String key) throws Exception	{	return getNode(key).getIntegerArray();	}
	
	// Setter
	public Node setValue(String s) 			{	this.sValue = s;	return this;	}
	public Node setParent(Node parent) 		{	this.parent = parent;	return this;	}
	public Node setType(int type) 			{	this.type = type;	return this;	}
	public Node setBoolean(Boolean b)		{	this.bValue = b;	this.type = NODE_TYPE_BOOLEAN;	return this;	}
	public Node setInteger(BigInteger i)	{	this.iValue = i;	this.type = NODE_TYPE_INTEGER;	return this;	}
	public Node setFloat(BigDecimal f) 		{	this.fValue = f;	this.type = NODE_TYPE_FLOAT;	return this;	}
	public Node setString(String s) 		{	this.sValue = cleanString(s);	this.type = NODE_TYPE_STRING;	return this;	}
	public Node setVariable(String s)		{	this.sValue = s;	this.type = NODE_TYPE_VARIABLE;	return this;	}
	public Node setPointer(String s)		{	this.sValue = s;	this.type = NODE_TYPE_POINTER;	return this;	}
	public Node setPointer(Pointer f) 		{	this.pointer = f;	this.type = NODE_TYPE_POINTER;	return this;	}
	public Node setList()			 		{	this.children = new ArrayList<Node>();	this.type = NODE_TYPE_LIST;	return this;	}
	public Node setVoid()			 		{	this.type = NODE_TYPE_VOID;	return this;	}
	public Node setInteger(Integer i)		{	return setInteger(BigInteger.valueOf(i));	}
	public Node setFloat(Double f) 			{	return setFloat(new BigDecimal(f));		}
	
	// Type control
	public boolean isFloat() 	{	return type == NODE_TYPE_FLOAT;		}
	public boolean isInteger()	{	return type == NODE_TYPE_INTEGER;	}
	public boolean isString()	{	return type == NODE_TYPE_STRING;	}
	public boolean isBoolean()	{	return type == NODE_TYPE_BOOLEAN;	}
	public boolean isList()		{	return type == NODE_TYPE_LIST;		}
	public boolean isVoid()		{	return type == NODE_TYPE_VOID;		}
	public boolean isVariable() {	return type == NODE_TYPE_VARIABLE;	}
	public boolean isPointer()	{	return type == NODE_TYPE_POINTER;	}
	
	// Start / Stop
	static public void start() 	{	alive = true;	}
	static public void stop() 	{	alive = false;	}
	
	// Run node ONLY if it is a pointer
	public Node run() throws Exception {
		if (type == Node.NODE_TYPE_POINTER && alive)
			if (pointer != null)
				pointer.run(this);
		return this;
	}
	
	// Children management
	public Node addChild(Node n) {
		children.add(n);
		n.parent = this;
		return this;
	}
	public Node insertChild(int index, Node value) {
		children.add(index, value);
		return this;
	}
	public Node deleteChild(Node n) {
		children.remove(n);
		return this;
	}
	public Node deleteChild(int i) {
		children.remove(i);
		return this;
	}
	
	// Other
	public Node copy(Node o) {
		//this.parent = ((o.parent!=null && this.parent==o.parent)?o.parent.copy():null); // stackoverflow
		this.type = o.type;
		this.bValue = o.bValue;
		this.fValue = o.fValue;
		this.iValue = o.iValue;
		this.sValue = o.sValue;
		this.pointer = o.pointer;
		if (o.children != null) {
			this.children = new ArrayList<Node>(); // purge current children
			for (Node n : o.children)
				this.addChild(n.copy());
		}
		return this;
	}
	public Node copy() {
		return new Node().copy(this);
	}
	/*public boolean isLeaf() {
		return type==NODE_TYPE_INTEGER || type==NODE_TYPE_FLOAT || type==NODE_TYPE_STRING;
	}*/
	
	/**
	 * Create a list containing the current node
	 */
	public Node list() {
		return new Node().setList().addChild(this);
	}
	
	/**
	 * Create a list containing 2 nodes
	 */
	public static Node list(Node n1, Node n2) {
		return new Node().setList().addChild(n1).addChild(n2);
	}
	
	/**
	 * Returns a double corresponding to the node
	 * Exception is thrown if node is not Float nor Integer
	 */
	public double getDoubleValue() throws Exception {
		if (isFloat())			return fValue.doubleValue();
		else if (isInteger())	return iValue.doubleValue();
		else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE); 
	}
	public double getIntValue() throws Exception {
		if (isInteger())		return iValue.intValue();
		else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE); 
	}

	 
	/**
	 * Returns the type as a string
	 */
	public String getTypeAsString() {
		switch (type) {
		case NODE_TYPE_BOOLEAN:			return "boolean";
		case NODE_TYPE_INTEGER:			return "integer";
		case NODE_TYPE_STRING:			return "string";
		case NODE_TYPE_FLOAT:			return "float";
		case NODE_TYPE_VARIABLE:		return "variable";
		case NODE_TYPE_VOID:			return "void";
		case NODE_TYPE_LIST:			return "list";
		case NODE_TYPE_POINTER:			return "pointer";
		default:						return "error";
		}
	}
	/**
	 * Converts to node to a boolean (user toBoolean(node.copy()) to have a new node)
	 */
	public Node toBoolean() {
		switch (type) {
		case NODE_TYPE_BOOLEAN:			break;
		case NODE_TYPE_INTEGER:			setBoolean(iValue.intValue()!=0);	break;
		case NODE_TYPE_STRING:			setBoolean(sValue.length()>0);		break;
		case NODE_TYPE_FLOAT:			setBoolean(fValue.doubleValue()!=0);break;
		case NODE_TYPE_VARIABLE:		setBoolean(false);					break;
		case NODE_TYPE_VOID:			setBoolean(false);					break;
		case NODE_TYPE_LIST:			setBoolean(false);					break;
		case NODE_TYPE_POINTER:			setBoolean(false);					break;
		}
		return this;
	}
	
	/**
	 * Compute a string representing the node
	 */
	public String toString() {
		switch (type) {
		case NODE_TYPE_BOOLEAN:			return (bValue!=null?(bValue.booleanValue()?"true":"false"):"undef");
		case NODE_TYPE_INTEGER:			return iValue.toString();
		case NODE_TYPE_STRING:			return '"'+sValue+'"';
		case NODE_TYPE_VARIABLE:		return sValue;
		case NODE_TYPE_VOID:			return "null";	
		
		case NODE_TYPE_FLOAT:
			BigDecimal bd = fValue.setScale(parser.getDecimalPrintPrecision(), parser.getRoundingMode()).stripTrailingZeros();
			if (parser.getEngineeringNotation())	return bd.toEngineeringString();
			else									return removeTrailingZeros(bd.toString());

		case NODE_TYPE_LIST:
			String s = "";
			for (Node n : children) s += n.toString()+",";
			if (s.length()==0)	return "{}";
			else 				return String.format("{%s}",s.substring(0,s.length()-1));
			
		case NODE_TYPE_POINTER:
			
			// Get function name from pointer
			if (sValue==null)
				sValue = parser.functionManager.getName(pointer);
			
			s = "";
			if (getChildrenCount()==0)	return sValue+"()";
			String symbol = parser.operatorManager.getSymbolFromName(sValue);
			
			// Replace function by operator
			if (symbol != null) {
				int flag = parser.operatorManager.getOperatorFromName(sValue).flag;
				
				// Build representation
				if (flag == Operator.LEFT_OPERAND + Operator.RIGHT_OPERAND) { // TODO optimize since operator function only take 1 or 2 params
					for (Node n : children) 	s += n.toString()+symbol;
					s = s.substring(0,s.length()-symbol.length());
				} else if (flag == Operator.LEFT_OPERAND)
					s = children.get(0).toString()+symbol;
				else if (flag == Operator.RIGHT_OPERAND)
					s = symbol+children.get(0).toString();
				
				// No need parenthesis is parent node is an operator with lower priority {k+8*p,8*p+k,k*(8+p)}
				if (parent != null) {
					if (parent.type == NODE_TYPE_LIST)	return s;
					if (parent.type == NODE_TYPE_POINTER)
						if (parser.operatorManager.compareFunctionPriorities(parent.sValue, sValue)==1)
							return s;
				} else return s;
				return String.format("(%s)",s);
				
			// Replace function by operator (child(a,b) => b[a])
			} else if (sValue.equals("getChild")) {
				return String.format("%s[%s]",children.get(1).toString(), parser.removeExtraParenthesis(children.get(0).toString()));
				
			// Replace function by operator (setchild(a,b,c) => b[a]=c)
			} else if (sValue.equals("setChild")) {
				return String.format("%s[%s]=%s",
						children.get(1).toString(),
						parser.removeExtraParenthesis(children.get(0).toString()),
						children.get(2).toString());
			
			// Leave function call as this
			} else {
				for (Node n : children) 	s += parser.removeExtraParenthesis(n.toString())+",";
				return String.format("%s(%s)", sValue,s.substring(0,s.length()-1));
			}
		}
		return "Error (type="+type+")";
	}
	
	
	/**
	 * Remove trailing zeros and decimal separator . is not necessary
	 * Example: 0.00000000 => 0
	 */
	private static String removeTrailingZeros(String s) {
		while (s.length()>1) {
			char c = s.charAt(s.length()-1);
			if (c=='0' || c=='.')	s = s.substring(0,s.length()-1);
			else return s;
		}
		return s;
	}
	
	/**
	 * Replaces \n by a line return \t by a tabulation
	 */
	private String cleanString(String s) {
		return s.replace("\\n", "\n").replace("\\t", "\t").replace("\\r", "\r");
	}
}

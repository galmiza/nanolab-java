package net.galmiza.nanolab.engine;


class Operator {
	
	// Flags
	static final int LEFT_OPERAND = 1;
	static final int RIGHT_OPERAND = 2;
			
	// Attributes
	String symbol;
	String name;
	int flag;
		
	// Constructor
	Operator(String symbol, String name, int flag) {
		this.symbol = symbol;
		this.name = name;
		this.flag = flag;
	}
}

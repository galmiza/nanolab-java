package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;


/**
 * Algorithms (if, for, while, foreach, break, continue, return)
 */
class _LibAlgorithm extends _LibTools {
	
	/**
	 * if(expression, action if true, action if false)
	 */
	static Pointer if_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node expression = parser.runNode(node.getChild(0));
					Node true_ = node.getChild(1);

					Boolean b = expression.toBoolean().getBoolean();
					if (b.booleanValue()) 	return parser.runNode(node.copy(true_));
					else 					return node.setVoid();
					
				} else if (node.getChildrenCount()==3) {
					Node expression = parser.runNode(node.getChild(0));
					Node true_ = node.getChild(1);
					Node false_ = node.getChild(2);

					Boolean b = expression.toBoolean().getBoolean();
					if (b.booleanValue()) 	return parser.runNode(node.copy(true_));
					else 					return parser.runNode(node.copy(false_));
					
				} else
					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_COUNT);
			}
		};
	}

	/**
	 * for(var, start, end, expression)
	 */
	static Pointer for_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				
				
				
				// FOREACH
				if (node.getChildrenCount()==3) { // for(var,list,expression)
					Node var = node.getChild(0);
					Node list = node.getChild(1);					
					Node expression = node.getChild(2);

					// If the list is a variable, it can be locally modified
					String name = null;
					if (list.isVariable()) name = list.getString();  
					
					// Process list
					parser.runNode(list);
					
					// Control type
					if (!var.isVariable())						throw new Exception(Error.EXECUTION_UNDEFINED_VARIABLE);
					if (!list.isList() && !list.isString())		throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Run foreach loop
					if (list.isList()) {
						for (int i=0; i<list.getChildrenCount(); i++) {
							if (break_!=null) break;
							if (continue_!=null) continue;
							if (return_ != null) break;
							parser.addVariable(var.getString(), list.getChild(i), 0); // n is a REFERENCE not a copy?
							parser.runNode(expression.copy()); // COPY so that the expression is not overwritten forever!
							if (name!=null)
								parser.getVariableFromName(name).getChild(i).copy(parser.runNode(var.copy()));
						}
					} else if (list.isString()) {
						for (int i=0; i<list.getString().length(); i++) {
							if (break_!=null) break;
							if (continue_!=null) continue;
							if (return_ != null) break;
							parser.addVariable(var.getString(), new Node(String.valueOf(list.getString().charAt(i))), 0); // n is a REFERENCE not a copy?
							parser.runNode(expression.copy()); // COPY so that the expression is not overwritten forever!
						}
					}
					
					
					break_=null;
					continue_=null;
					return node.setVoid();
					
					
					
				// FOR no step
				} else if (node.getChildrenCount()==4) { // for(var,from,to,expression)
					Node var = node.getChild(0);
					Node from = parser.runNode(node.getChild(1));
					Node to = parser.runNode(node.getChild(2));
					Node expression = node.getChild(3);
					
					// Control types
					if (!var.isVariable())						throw new Exception(Error.EXECUTION_UNDEFINED_VARIABLE);
					if(!from.isInteger() || !to.isInteger())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
						
					
					// Run for loop
					int from_ = from.getInteger().intValue();
					int to_ = to.getInteger().intValue();
					for (int i=from_; i<=to_; i++) {
						if (break_!=null) break;
						if (continue_!=null) continue;
						if (return_ != null) break;
						parser.addVariable(var.getString(), new Node(i), 0);
						parser.runNode(expression.copy()); // COPY so that the expression is not overwritten forever!
					}
					
					break_=null;
					continue_=null;
					return node.setVoid(); // return blank
					
					
					
				// FOR step
				} else if (node.getChildrenCount()==5) { // for(var,from,to,step,expression)
					Node var = node.getChild(0);
					Node from = parser.runNode(node.getChild(1));
					Node to = parser.runNode(node.getChild(2));
					Node step = parser.runNode(node.getChild(3));
					Node expression = node.getChild(4);
					
					// Control types
					if (!var.isVariable())						throw new Exception(Error.EXECUTION_UNDEFINED_VARIABLE);
					if (!from.isInteger() && !from.isFloat())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					if (!to.isInteger() && !to.isFloat())		throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					if (!step.isInteger() && !step.isFloat())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Run for loop
					float from_ = from.isFloat() ? from.getFloat().floatValue() : from.getInteger().floatValue();
					float to_ = to.isFloat() ? to.getFloat().floatValue() : to.getInteger().floatValue();
					float step_ = step.isFloat() ? step.getFloat().floatValue() : step.getInteger().floatValue();
					for (float f=from_; f<=to_; f+=step_) {
						if (break_!=null) break;
						if (continue_!=null) continue;
						if (return_ != null) break;
						parser.addVariable(var.getString(), new Node(Double.valueOf(f)), 0);
						parser.runNode(expression.copy()); // COPY so that the expression is not overwritten forever!
					}
					
					break_=null;
					continue_=null;
					return node.setVoid(); // return blank
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 4));
			}
		};
	}
	
	/**
	 * while(eval, expression)
	 */
	static Pointer while_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node eval = node.getChild(0);
					Node expression = node.getChild(1);
					
					while (parser.runNode(eval.copy()).toBoolean().getBoolean()) {
						if (break_ != null) 	{ break_ = null; 	break; }
						if (continue_ != null)	{ continue_ = null;	continue; }
						if (return_ != null)	{ break; }
						parser.runNode(expression.copy()); // COPY so that the expression is not overwritten forever!
					}
					return node.setVoid(); // return blank
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}

	/**
	 * break()
	 */
	static Node break_;
	static Pointer break_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==0) {
					break_ = new Node(); // set different from null
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 0));
			}
		};
	}
	
	/**
	 * continue()
	 * TODO doesnt work since all items of list are run before the control of continue_
	 */
	static Node continue_;
	static Pointer continue_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==0) {
					continue_ = new Node(); // set different from null
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 0));
			}
		};
	}
	
	/**
	 * return(node)
	 */
	static Node return_;
	static Pointer return_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) { // for(var,from,to,expression)
					return_ = node.getChild(0).copy();
					return node.copy(return_);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
}

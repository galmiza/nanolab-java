package net.galmiza.nanolab.engine;


import net.galmiza.nanolab.engine.Function.Pointer;

class _LibLogical extends _LibTools {

	/**
	 * Logical
	 * 
	 * Manages:
	 *  boolean vs boolean
	 *  
	 * Manages:
	 *  list vs boolean
	 *  boolean vs list
	 *  list vs list (with matching dimensions)
	 *  
	 * Ignore other combinations
	 * 
	 */

	private static final int LOGICAL_AND = 0;
	private static final int LOGICAL_OR = 1;
	private static final int LOGICAL_XOR = 2;
	
	
	static Pointer and() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return logic(node, this, LOGICAL_AND); }
		};
	}
	static Pointer or() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return logic(node, this, LOGICAL_OR); }
		};
	}
	static Pointer xor() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return logic(node, this, LOGICAL_XOR); }
		};
	}
	
	
	/**
	 * Generic function to be used for all comparison operators
	 */
	static Node logic(Node node, Pointer f, int comparator) throws Exception {
		if (node.getChildrenCount()==2) {
			Node n1 = node.getChild(0);
			Node n2 = node.getChild(1);
			if (n1.isList() || n2.isList())
				return node.copy(recursive2(n1, n2, f));
			else
				return node.setBoolean(compareNodes(comparator, node.getChild(0), node.getChild(1)));
		} else throw new Exception(parser.getArgumentCountExceptionString(f, 2));
	}
	
	/**
	 * Return the boolean corresponding to the comparison of two nodes
	 */
	static Boolean compareNodes(int type, Node n1, Node n2) throws Exception {
		if (n1.isBoolean() && n2.isBoolean()) {
			if (type == LOGICAL_AND)	return new Boolean(n1.getBoolean() && n2.getBoolean());
			if (type == LOGICAL_OR)		return new Boolean(n1.getBoolean() || n2.getBoolean());
			if (type == LOGICAL_XOR)	return new Boolean(n1.getBoolean() ^  n2.getBoolean());
			throw new Exception(Error.EXECUTION_INTERNAL_ERROR);
		} else
			throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
	}
}

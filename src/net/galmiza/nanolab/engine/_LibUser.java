package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



class _LibUser extends _LibTools {

	
	/**
	 * User defined function 
	 */
	static Pointer user(final Node args, final Node func) {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {

				// Check argument count is ok
				if (node.getChildrenCount()!=args.getChildrenCount())
					throw new Exception(parser.getArgumentCountExceptionString(this, args.getChildrenCount()));
				
				// Change scope (variables created with highest scope are returned first by getVariable)
				parser.increaseVariableScope();
				
				// Map arguments from node (input) to variables (args)
				for (int i=0; i<args.getChildrenCount(); i++)
					parser.addVariable(args.getChild(i).getString(), new Node().copy(node.getChild(i)), 0);
	
				// Run expression on a COPY so that value is not overwritten
				Node tmp = func.copy();
				parser.runNode(tmp);
				
				// Manage return of the user input function
				if (_LibAlgorithm.return_!=null) {
					node.copy(_LibAlgorithm.return_);
					_LibAlgorithm.return_ = null;
				}
				else	node.copy(tmp);
				
				// Destroy all variables from the scope of this user function execution
				parser.removeAllVariables(parser.getVariableScope());
				parser.decreaseVariableScope();
				
				return node;
			}
		};
	}
}

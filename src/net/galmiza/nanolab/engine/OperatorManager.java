package net.galmiza.nanolab.engine;

import java.util.ArrayList;
import java.util.List;


class OperatorManager {

	// Attributes
	private List<Operator> operators;
	
	// Constructor
	OperatorManager() {
		operators = new ArrayList<Operator>();
	}
	
	// Add
	void add(String symbol, String name, int flag) {
		removeFromSymbol(symbol);
		operators.add(new Operator(symbol, name, flag));
	}

	// Get
	List<Operator> getOperators() {
		return operators;
	}
	Operator getOperatorFromSymbol(String symbol) {
		for (Operator o : operators)
			if (o.symbol.equals(symbol))
				return o;
		return null;
	}
	Operator getOperatorFromName(String name) {
		for (Operator o : operators)
			if (o.name.equals(name))
				return o;
		return null;
	}
	String getSymbolFromName(String name) {
		for (Operator o : operators)
			if (o.name.equals(name))
				return o.symbol;
		return null;
	}
	
	// Remove
	void removeFromSymbol(String symbol) {
		Operator o = getOperatorFromSymbol(symbol);
		if (o != null) operators.remove(o);		
	}
	
	// Compare priority (same:0, higher:1, lower:-1)
	int compareFunctionPriorities(String f1, String f2) {
		int i1 = operators.indexOf(getOperatorFromName(f1));
		int i2 = operators.indexOf(getOperatorFromName(f2));
		if (i1==i2) return 0;
		if (i1>i2) return 1;
		return -1;
	}
}

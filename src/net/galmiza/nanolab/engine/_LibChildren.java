package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



class _LibChildren extends _LibTools {
	
	/**
	 * Children
	 */
	static Pointer getChild() { // getChild({index}, list)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node index = node.getChild(0);
					Node list = node.getChild(1);
					
					parser.runNode(index); // resolve variables
					parser.runNode(list); // resolve variables
					
					// list is a LIST
					if (list.isList()) {
						
						// Child referenced by an index
						if (index.isInteger()) {
							return node.copy(list.getChild(index.getInteger().intValue()));
							
						// Child referenced by a list of index
						} else if (index.isList()) {
							if (index.getChildrenCount()>0) {
								Node i = index.getChildren().remove(0); // get first child
								
								// Extract integer
								if (i.isInteger()) {
									list.copy(list.getChild(i.getInteger().intValue()));
									if (index.getChildrenCount()==0) {
										index.copy(i);
										node.copy(list);
									} else
										getChild().run(node);
									
								// Extract string TODO reduce size?
								} else if (i.isString()) {
									boolean found = false;
									for (Node n : list.getChildren())
										if (n.isList())
											if (n.getChildrenCount()==2)
												if (n.getChild(0).isString())
													if (n.getChild(0).getString().equals(i.getString())) {
														list.copy(n.getChild(1));
														found = true;
													}
									if (!found)		return node.setVoid();
									else
										if (index.getChildrenCount()==0) {
											index.copy(i);
											node.copy(list);
										} else
											getChild().run(node);
									
								} else
									throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
																
							} else
								return node;
							
						// Child referenced by a string (like an associative array)
						} else if (index.isString()) {
							for (Node n : list.getChildren())
								if (n.isList())
									if (n.getChildrenCount()==2)
										if (n.getChild(0).isString())
											if (n.getChild(0).getString().equals(index.getString()))
												return node.copy(n.getChild(1));
							return node.setVoid();
						} else
							throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
						
					// list is a STRING
					} else if (list.isString()) {
						if (index.isInteger())
							return node.setString(list.getString().charAt(index.getInteger().intValue())+""); // concat to an empty string to force cast to string
						else
							throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					} else if (!list.isVariable())
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					return node;
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	
	
	static Pointer setChild() { // setchild({index}, list, value), returns result after update
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==3) {
					Node index = node.getChild(0);
					Node list = node.getChild(1);
					Node value = node.getChild(2);

					parser.runNode(index); // resolve variables
					parser.runNode(value); // resolve variables
					
					
					// Child referenced by an index
					if (index.isInteger()) {
						int i = index.getInteger().intValue();
						Node tmp = parser.getVariableFromName(list.getString()); // get node of variable
						if (tmp!=null) {
							if (tmp.isList()) {
								tmp.getChild(i).copy(value);
								return node.copy(tmp);
							} else if (tmp.isString()) {
								String s = value.isString() ? value.getString() : value.toString();
								String o = tmp.getString().substring(0,i)+s+tmp.getString().substring(i+1);
								return node.copy(tmp.setString(o));
							} else
								throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
						} else
							throw new Exception(Error.EXECUTION_UNDEFINED_VARIABLE);
						
						
					// Child referenced by a list of index
					} else if (index.isList()) {
						if (index.getChildrenCount()==0)
							return node.copy(addChild().run(Node.list(list, value)));
						else {
							Node tmp = parser.getVariableFromName(list.getString()); // get node of variable
							
							// Browse into node of variable
							for (Node n : index.getChildren())
								if (n.isInteger())
									tmp = tmp.getChild(n.getInteger().intValue());
							
							if (index.getChild(index.getChildrenCount()-1).isList()) // is last item is a list, we have a list[1][2][]=value situation
								tmp.addChild(value);
							else
								tmp.copy(value);
							
							return node.copy(parser.getVariableFromName(list.getString()));
						}
						
						
					// Child referenced by a string (like an associative array)
					} else if (index.isString()) { // a={"hey":6,"salut":"yo"}
						Node tmp = parser.getVariableFromName(list.getString()); // get node of variable
						rmChild().run(Node.list(index, list)); // remove keys with same name
						tmp.addChild(new Node().setList().addChild(new Node(index.getString())).addChild(value));
						return node.copy(tmp);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 3));
			}
		};
	}
	
	static Pointer addChild() { // addchild(list,value), returns updated list after update
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node list = node.getChild(0);
					Node value = node.getChild(1);

					parser.runNode(value); // resolve variables
					
					Node tmp = parser.getVariableFromName(list.getString()); // get node of variable
					if (tmp != null)
						if (tmp.isList()) {
							tmp.addChild(value);
							return node.copy(tmp);
						} else
							throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	static Pointer insChild() { // inschild({index},list,value)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==3) {
					Node index = node.getChild(0);
					Node list = node.getChild(1);
					Node value = node.getChild(2);

					parser.runNode(index); // resolve variables
					parser.runNode(value); // resolve variables
					
					if (index.isInteger()) {
						Node tmp = parser.getVariableFromName(list.getString()); // get node of variable
						if (tmp != null)
							if (tmp.isList()) {
								tmp.insertChild(index.getInteger().intValue(), value);
								return node.copy(tmp);
							} else
								throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
						else
							throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					} else
						throw new Exception(parser.getArgumentCountExceptionString(this, 3));
				} else
					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
			}
		};
	}
	
	static Pointer rmChild() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node index = parser.runNode(node.getChild(0));
					Node list = node.getChild(1);
					
					// Child referenced by an index
					if (index.isInteger()) {
						Node tmp = parser.getVariableFromName(list.getString());
						tmp.deleteChild(index.getInteger().intValue());
						return node.copy(tmp);
					
					// Child referenced by a string (case of associative arrays)
					} else if (index.isString()) {
						Node tmp = parser.getVariableFromName(list.getString());
						for (Node n : tmp.getChildren())
							if (n.isList())
								if (n.getChildrenCount()==2)
									if (n.getChild(0).isString())
										if (n.getChild(0).getString().equals(index.getString()))
											return node.copy(tmp.deleteChild(n));
						return node.copy(tmp);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
}

package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



/**
 * String manipulation
 */
class _LibString extends _LibTools {
	
	static Pointer charAt() { // charAt(index, string)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node i = parser.runNode(node.getChild(0));
					Node s = parser.runNode(node.getChild(1));
					
					if (i.isInteger() && s.isString())
						return node.setString(String.valueOf(s.getString().charAt(i.getInteger().intValue())));
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer indexOf() { // indexOf(find, string)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node f = parser.runNode(node.getChild(0));
					Node s = parser.runNode(node.getChild(1));
					
					if (s.isString() && f.isString())
						return node.setInteger(s.getString().indexOf(f.getString()));
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer split() { // split(separator, string)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node sep = parser.runNode(node.getChild(0));
					Node str = parser.runNode(node.getChild(1));
					
					if (sep.isString() && str.isString()) {
						node.setList();
						for (String s : str.getString().split(sep.getString()))
							node.addChild(new Node(s));
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer substring() { // substring(start, length, string)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==3) {
					Node start = parser.runNode(node.getChild(0));
					Node length = parser.runNode(node.getChild(1));
					Node s = parser.runNode(node.getChild(2));
					
					if (start.isInteger() && length.isInteger() && s.isString())
						return node.setString(s.getString().substring(start.getInteger().intValue(), length.getInteger().intValue()+start.getInteger().intValue()));
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 3));
			}
		};
	}
	static Pointer replace() { // replace(find, replace, string)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==3) {
					Node find = parser.runNode(node.getChild(0));
					Node replace = parser.runNode(node.getChild(1));
					Node s = parser.runNode(node.getChild(2));

					if (find.isString() && replace.isString() && s.isString())
						return node.setString(s.getString().replace(find.getString(), replace.getString()));
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 3));
			}
		};
	}
}

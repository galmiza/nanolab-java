package net.galmiza.nanolab.engine;

import java.io.Serializable;


public class Variable implements Serializable {

	// Version
	private static final long serialVersionUID = 1L;

	// Constant for flags
	final static int TYPE_BUILT_IN = 1;
	
	// Attribute
	public String name;
	public Node value;
	public int flag;
	public int scope;
	
	// Constructor
	Variable(String name, Node value, int flag, int scope) {
		this.name = name;
		this.value = value;
		this.flag = flag;
		this.scope = scope;
	}
	
}

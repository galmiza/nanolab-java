package net.galmiza.nanolab.engine;

import java.util.ArrayList;
import java.util.List;

import net.galmiza.nanolab.engine.Function.Pointer;

class FunctionManager{
	
	// Attributes
	List<Function> functions;
	
	// Constructor
	FunctionManager() {
		functions = new ArrayList<Function>();
	}

	// Add
	void add(String name, Pointer pointer, int flag) throws Exception {
		remove(name);
		functions.add(new Function(name, pointer, flag));
	}
	void add(String name, Node target, Node expression, int flag) throws Exception {
		remove(name);
		Function f = new Function(name, _LibUser.user(target, expression), flag);
		f.target = target;
		f.expression = expression;
		functions.add(f);
	}
	
	// Get
	Function getFunction(String name) {
		for (Function f : functions)
			if (f.name.equals(name))
				return f;
		return null;
	}
	String getName(Pointer f) {
		for (Function f_ : functions)
			if (f_.pointer == f)
				return f_.name;
		return null;
	}
	Pointer getPointer(String name) {
		Function f = getFunction(name);
		if (f!=null) return f.pointer;
		return null;
	}
	Function getFunction(Pointer pointer) {
		for (Function f : functions)
			if (f.pointer == pointer)
				return f;
		return null;
	}
	
	// Remove
	void remove(String name) throws Exception {
		
		// Prevent from using core functions
		Function f = getFunction(name);
		if (f!=null)
			if ((f.flag & Variable.TYPE_BUILT_IN)>0)
				throw new Exception(Error.FUNCTIONS_PROTECTED_BUILT_IN);
		
		functions.remove(getFunction(name));
	}

	// Return a list of functions filtered on scripted functions
	List<Function> getScriptedFunction() {
		List<Function> l = new ArrayList<Function>();
		for (Function f : functions) {
			if ((f.flag & Function.TYPE_SCRIPTED)>0)
				l.add(f);
		}
		return l;
	}
}
	
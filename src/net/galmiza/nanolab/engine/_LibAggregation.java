package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



/**
 * 'count' only
 * All other aggregation functions are scripted!
 */
class _LibAggregation extends _LibTools {
		
	static Pointer count() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isList())		return node.setInteger(child.getChildrenCount());
					if (child.isString())	return node.setInteger(child.getString().length());
					if (child.isVariable())	throw new Exception(Error.EXECUTION_UNDEFINED_VARIABLE);
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
}

package net.galmiza.nanolab.engine;


import java.math.BigDecimal;

import net.galmiza.nanolab.engine.Function.Pointer;

/**
 * Abstract class for all static classes that define functions
 * (object Function attribute of Nodes)
 * For clarity, these static classes are defined under package /functions
 */
public abstract class _LibTools {
		
	// References
	public static Parser parser; // to access workspace variables
	

	/**
	 * Class to describe a function prototype (name, arg count, node function)
	 */
	static abstract class AnyFunction {
		
		// Attributes
		String name;
		int argc;
		Pointer func;
		
		// Constructor
		AnyFunction(String name, int argc, Pointer func) {
			this.name = name;
			this.argc = argc;
			this.func = func;
		}
	}
	
	
	// Prototype for a function that takes a double and returns a double
	static abstract class DDFnc extends AnyFunction {
        DDFnc(String name, Pointer func) {	super(name, 1, func);	}
		abstract double run(double d) throws Exception;
    }
	// Prototype for a function that takes a 2 doubles and returns a double
	static abstract class DD2Fnc extends AnyFunction {
		DD2Fnc(String name, Pointer func) {	super(name, 2, func);	}
		abstract double run(double d1, double d2) throws Exception;
    }
	
	// Method to process node associated with 'double f(double)' functions 
	static Node processNode(Node node, DDFnc f) throws Exception {
		if (node.getChildrenCount()==1) {
			Node child = node.getChild(0);
			if (child.isFloat())	{ return node.setFloat(f.run(child.getFloat().doubleValue())); }
			if (child.isInteger())	{ return node.setFloat(f.run(child.getInteger().doubleValue())); }
			if (child.isList()) {
				Node tmp = new Node();
				tmp.setList();
				for (Node n : child.getChildren()) 
					tmp.addChild(n.list().setPointer(f.func).run());
				return node.copy(tmp);
			}
			return node;
		} else
			throw new Exception(parser.getArgumentCountExceptionString(f.func, f.argc));
	}
	
	// Method to process node associated with 'double f(double,double)' functions
	static Node processNode(Node node, DD2Fnc f) throws Exception {
		if (node.getChildrenCount()==2) {
			Node n1 = node.getChild(0);
			Node n2 = node.getChild(1);
			
			Node tmp = new Node().setList();
			if (n1.isFloat()) {
				if (n2.isFloat())	return node.setFloat(f.run(n1.getFloat().doubleValue(), n2.getFloat().doubleValue()));
				if (n2.isInteger())	return node.setFloat(f.run(n1.getFloat().doubleValue(), n2.getInteger().doubleValue()));
				if (n2.isList())
					for (Node n : n2.getChildren()) 
						tmp.addChild(Node.list(n1, n).setPointer(f.func).run());
			}
			else if (n1.isInteger()) {
				if (n2.isFloat())	return node.setFloat(f.run(n1.getInteger().doubleValue(), n2.getFloat().doubleValue()));
				if (n2.isInteger())	return node.setFloat(f.run(n1.getInteger().doubleValue(), n2.getInteger().doubleValue()));
				if (n2.isList())
					for (Node n : n2.getChildren()) 
						tmp.addChild(Node.list(n1, n).setPointer(f.func).run());
			}
			else if (n1.isList()) {
				if (n2.isFloat() || n2.isInteger())
					for (Node n : n1.getChildren()) 
						tmp.addChild(Node.list(n, n2).setPointer(f.func).run());
				if (n2.isList())
					for (int i=0; i<n1.getChildrenCount(); i++)
						tmp.addChild(Node.list(n1.getChild(i), n2.getChild(i)).setPointer(f.func).run());
			} else
				return node;
			return node.copy(tmp);
		} else
			throw new Exception(parser.getArgumentCountExceptionString(f.func, f.argc));
	}
	
	
	
	/**
	 * Generic functions for recursive calls
	 */
	static Node recursive1(Node node, Pointer f) throws Exception {
		Node tmp = new Node().setList();
		for (Node n : node.getChildren())
			tmp.addChild(n.list().setPointer(f).run());
		return tmp;
	}
	static Node recursive2(Node n1, Node n2, Pointer f) throws Exception {
		Node tmp = new Node().setList();
		if (n1.isList() && n2.isList()) {
			if (n1.getChildrenCount() == n2.getChildrenCount())
				for (int i=0; i<n1.getChildrenCount(); i++)
					tmp.addChild(f.run(Node.list(n1.getChild(i),n2.getChild(i))));
			else
				throw new Exception(Error.EXECUTION_DIMENSION_MISMATCH);
		}
		else if (n1.isList())		for (Node n : n1.getChildren())	tmp.addChild(Node.list(n, n2).setPointer(f).run());
		else if (n2.isList())		for (Node n : n2.getChildren())	tmp.addChild(Node.list(n1, n).setPointer(f).run());
		return tmp;
	}
	
	
	/**
	 * Floating point number precision nl functions
	 */
	static Pointer setDecimalPrintPrecision() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isInteger()) {
						parser.setDecimalPrintPrecision(child.getInteger().intValue());
						return node.copy(child);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}	
	static Pointer setDecimalExecutionPrecision() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isInteger()) {
						parser.setDecimalExecutionPrecision(child.getInteger().intValue());
						return node.copy(child);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}	
	
	/**
	 * Functions for mapping Nodes to Matrix (for lib Jama)
	 */
	static double[][] getValuesFromNode(Node n) throws Exception {
		boolean vector = false;
		
		int d1 = 0;
		if (n.isList())
			d1 = n.getChildrenCount(); // count in dimension 1s
		if (d1==0) return new double[0][0];
		
		int d2 = 0;
		if (n.getChild(0).isList())
			d2 = n.getChild(0).getChildrenCount(); // count in dimension 2
		if (d2==0) vector = true; // no 2nd dimension => matrix is a vector
		
		// Allocate data for matrix / vector
		double[][] m = new double[d1][vector?1:d2];
		for (int i=0; i<d1; i++) {
			Node child = n.getChild(i);
			if (vector) {	// vector
				if 		(child.isFloat())	m[i][0] = child.getFloat().doubleValue();
				else if (child.isInteger())	m[i][0] = child.getInteger().doubleValue();
			}
			else {		// matrix
				for (int j=0; j<d2; j++) {
					if 		(child.getChild(j).isFloat())	m[i][j] = child.getChild(j).getFloat().doubleValue();
					else if (child.getChild(j).isInteger())	m[i][j] = child.getChild(j).getInteger().doubleValue();
				}
			}
		}
		//printMatrix(m);
		return m;
	}

	// TODO warning matrix 3 dimensions
	static Node getNodeFromValues(double[][] m) throws Exception {
		Node n = new Node();
		n.setList();
		int d1 = m.length;
		if (d1==0) return n;
		int d2 = m[0].length;
		
		for (int i=0; i<d1; i++) {
			Node child = new Node();
			n.addChild(child);
			if (d2>1) { // matrix
				child.setList();
				for (int j=0; j<d2; j++)
					child.addChild(new Node().setFloat(new BigDecimal(m[i][j])));
			} else { // vector
				child.setFloat(new BigDecimal(m[i][0]));
			}

		}
		return n;
	}
	
	/**
	 * DEBUG print matrix double[][]
	 */
	/*static void printMatrix(double[][] m) {
		if (m.length>0) {
			for (int i=0; i<m.length; i++)
				for (int j=0; j<m[0].length; j++)
					System.out.println(String.format("(%d,%d) = %f", i,j,(float)m[i][j]));
		}
	}*/
}

package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



class _LibDefinition extends _LibTools {

	/**
	 * Variables or functions
	 */
	static Pointer set() { // set(target,expressio)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node target = node.getChild(0); // can represent a variable or a function definition (e.g.  'a' for a variable,  'f(p,q)' for a function)
					Node expression = node.getChild(1); // represent the expression (right of =)
					
					// Variable definition
					if (target.isVariable()) {
						parser.runNode(expression);
						parser.addVariable(target.getString(), expression.copy(), 0);
						return node.copy(expression);
						
						
					// Function definition
					} else if (target.isPointer()) {
						
						// Check all arguments (children of this node) are variables
						for (int i=0; i<target.getChildrenCount(); i++)
							if (!target.getChild(i).isVariable())
								throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);

						parser.addUserFunction(target.getString(), target, expression, Function.TYPE_SCRIPTED);
						return node.setVariable(String.format("Function %s defined", target.getString()));
						
					// Multiple definition {a,b}={1,2}
					} else if(target.isList()) {
						parser.runNode(expression);
						if (target.getChildrenCount()==expression.getChildrenCount()) {
							for (int i=0; i<target.getChildrenCount(); i++)
								set().run(Node.list(target.getChild(i),expression.getChild(i)));
							return node.copy(expression);
						} else
							throw new Exception(Error.EXECUTION_DIMENSION_MISMATCH);
						
					// Error
					} else
						throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	
	
	
	
	
	static Pointer unset() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node key = node.getChild(0);
					if (key.isVariable()) {
						parser.removeVariable(key.getString());
						return node.setVariable(key.getString());
					} else
						throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	static Pointer getType() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)
					return node.setString(node.getChild(0).getTypeAsString());					
				else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	static Pointer call() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node name = node.getChild(0);
					Node args = node.getChild(1);
					if (name.isString() && args.isList()) {
						Pointer p = parser.getFunctionPointer(name.getString());
						if (p!=null)	return node.copy(p.run(args));
						else			throw new Exception(Error.EXECUTION_UNDEFINED_FUNCTION);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);				
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}	

	
	static Pointer run() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0); 
					if (child.isString())	return node.setString(parser.runExpression(child.getString()));
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else	throw new Exception(parser.getArgumentCountExceptionString(this, 0));
			}
		};
	}	
	
	static Pointer execute() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					parser.runCode(child.getString());
					if (child.isString())	return node.setVoid();
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else	throw new Exception(parser.getArgumentCountExceptionString(this, 0));
			}
		};
	}
}

package net.galmiza.nanolab.engine;

import java.io.Serializable;


/**
 * Function is the most conceptual class representing a function
 * Pointer is a more real function class since it represent a static existing java function
 *
 */
public class Function implements Serializable {

	// Version
	private static final long serialVersionUID = 1L;
	
	// Flags
	public static final int TYPE_BUILT_IN = 1;
	public static final int TYPE_SCRIPTED = 2;
	public static final int BLOCK_RECURSIVE_EXECUTION = 4;
		
	// Attributes
	public String name;
	transient Pointer pointer; // transient because cannot be serialized
	public Node target, expression;
	public int flag;
	
	// Constructor
	Function(String name, Pointer pointer, int flag) {
		this.name = name;
		this.pointer = pointer;
		this.flag = flag;
	}
	
	/**
	 * Class whose objects are attached to Nodes
	 * Can represent any function, whatever argument count and types
	 */
	public static abstract class Pointer {
        public abstract Node run(final Node node) throws Exception;
    }
}

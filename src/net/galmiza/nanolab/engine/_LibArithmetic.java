package net.galmiza.nanolab.engine;


import java.math.BigDecimal;
import java.math.RoundingMode;

import net.galmiza.nanolab.engine.Function.Pointer;
import Jama.Matrix;

/**
 * Define functions behavior for operators
 * add +
 * sub -
 * mul *
 * div /
 * pow ^
 */
class _LibArithmetic extends _LibTools {
	
	/**
	 * Operator +
	 */
	static Pointer add() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChildren().get(0);
					Node n2 = node.getChildren().get(1);
					
					// Scalar
					if (n1.isFloat() && n2.isFloat())		return node.setFloat(n1.getFloat().add(n2.getFloat()));
					if (n1.isInteger() && n2.isFloat())		return node.setFloat(n2.getFloat().add(new BigDecimal(n1.getInteger())));
					if (n1.isFloat() && n2.isInteger())		return node.setFloat(n1.getFloat().add(new BigDecimal(n2.getInteger())));
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().add(n2.getInteger()));
					if (n1.isString() && n2.isString())		return node.setString(n1.getString()+n2.getString());
					
					// Variables or functions
					if (n1.isVariable() || n2.isVariable())	return node;
					if (n1.isPointer() || n2.isPointer())	return node;
					
					// Wrong types
					if (n1.isBoolean() || n2.isBoolean())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Lists
					return node.copy(recursive2(n1, n2, this));
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	
	/**
	 * Operator *
	 * MAtrix multiplication is suitable
	 */
	static Pointer mul() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = parser.runNode(node.getChildren().get(0));
					Node n2 = parser.runNode(node.getChildren().get(1));
					
					// Scalar
					if (n1.isFloat() && n2.isFloat())		return node.setFloat(n1.getFloat().multiply(n2.getFloat()));
					if (n1.isInteger() && n2.isFloat())		return node.setFloat(n2.getFloat().multiply(new BigDecimal(n1.getInteger())));
					if (n1.isFloat() && n2.isInteger())		return node.setFloat(n1.getFloat().multiply(new BigDecimal(n2.getInteger())));
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().multiply(n2.getInteger()));
					if (n1.isVariable() || n2.isVariable())	return node;
					
					// Variables or functions
					if (n1.isVariable() || n2.isVariable())	return node;
					if (n1.isPointer() || n2.isPointer())	return node;
					
					// Wrong types
					if (n1.isBoolean() || n2.isBoolean())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					if (n1.isString() || n2.isString())		throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Handle matrix multiplication if suitable
					if (n1.isList() && n2.isList()) {
						Node tmp = new Node().setList();
						
						int c1 = n1.getChildrenCount();
						int c2 = n2.getChildrenCount();
						
						// One empty lead to empty result
						if (c1==0 || c2==0) {
							tmp.copy(new Node().setList());
						} else {
							// All vectors => item by item multiplication
							Pointer pSize = parser.getFunctionPointer("size");
							if (pSize.run(n1.list()).getChildrenCount()==1 && pSize.run(n2.list()).getChildrenCount()==1)
								for (int i=0; i<c1; i++)
									tmp.addChild(Node.list(n1.getChild(i), n2.getChild(i)).setPointer(mul()).run().setPointer("mul").run());									
							else {
								double[][] m1 = getValuesFromNode(n1);
								double[][] m2 = getValuesFromNode(n2);
							    Matrix M1 = new Matrix(m1);
							    Matrix M2 = new Matrix(m2);					    
							    M1 = M1.times(M2);
							    tmp = getNodeFromValues(M1.getArray());								
							}
						}
						return node.copy(tmp);
					}

					// Lists
					return node.copy(recursive2(n1, n2, this));
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}

		};
	}
	
	
	/**
	 * Operator -
	 */
	static Pointer sub() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChildren().get(0);
					Node n2 = node.getChildren().get(1);
					
					// Scalar
					if (n1.isFloat() && n2.isFloat())		return node.setFloat(n1.getFloat().subtract(n2.getFloat()));
					if (n1.isInteger() && n2.isFloat())		return node.setFloat(new BigDecimal(n1.getInteger()).subtract(n2.getFloat()));
					if (n1.isFloat() && n2.isInteger())		return node.setFloat(n1.getFloat().subtract(new BigDecimal(n2.getInteger())));
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().subtract(n2.getInteger()));
					if (n1.isVariable() || n2.isVariable())	return node;
					
					// Variables or functions
					if (n1.isVariable() || n2.isVariable())	return node;
					if (n1.isPointer() || n2.isPointer())	return node;

					// Wrong types
					if (n1.isBoolean() || n2.isBoolean())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					if (n1.isString() || n2.isString())		throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Lists
					return node.copy(recursive2(n1, n2, this));
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	
	/**
	 * Operator /
	 * Force conversion to float even for int/int
	 */
	static Pointer div() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChildren().get(0);
					Node n2 = node.getChildren().get(1);
					
					// Set precision, needed for division
					int p = parser.getDecimalExecutionPrecision();
					RoundingMode m = parser.getRoundingMode();
					
					// Scalar
					if (n1.isFloat() && n2.isFloat())		return node.setFloat(n1.getFloat().divide(n2.getFloat(),p,m));
					if (n1.isInteger() && n2.isFloat())		return node.setFloat(new BigDecimal(n1.getInteger()).divide(n2.getFloat(),p,m));
					if (n1.isFloat() && n2.isInteger())		return node.setFloat(n1.getFloat().divide(new BigDecimal(n2.getInteger()),p,m));
					if (n1.isInteger() && n2.isInteger())	return node.setFloat(new BigDecimal(n1.getInteger()).divide(new BigDecimal(n2.getInteger()),p,m));
					if (n1.isVariable() || n2.isVariable())	return node;
					
					// Variables or functions
					if (n1.isVariable() || n2.isVariable())	return node;
					if (n1.isPointer() || n2.isPointer())	return node;

					// Wrong types
					if (n1.isBoolean() || n2.isBoolean())	throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					if (n1.isString() || n2.isString())		throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Lists
					return node.copy(recursive2(n1, n2, this));
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	
	/**
	 * Increment / decrement
	 */
	static Pointer inc() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					if (node.getChild(0).isVariable()) {
						Node var = parser.getVariableFromName(node.getChild(0).getString());
						Node tmp = Node.list(var,new Node(1));
						var.copy(add().run(tmp));
						return node.copy(tmp);
					} else
						throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer dec() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					if (node.getChild(0).isVariable()) {
						Node var = parser.getVariableFromName(node.getChild(0).getString());
						Node tmp = Node.list(var,new Node(1));
						var.copy(sub().run(tmp));
						return node.copy(tmp);
					} else
						throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer indirect(final Pointer p) {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					if (node.getChild(0).isVariable()) {
						Node var = parser.getVariableFromName(node.getChild(0).getString());
						Node tmp = Node.list(var,node.getChild(1));
						p.run(parser.runNode(tmp));
						var.copy(tmp);
						return node.copy(tmp);
					} else
						throw new Exception(Error.EXECUTION_VARIABLE_EXPECTED);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer addi() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return indirect(add()).run(node);		}};}
	static Pointer subi() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return indirect(sub()).run(node);		}};}
	static Pointer muli() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return indirect(mul()).run(node);		}};}
	static Pointer divi() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {	return indirect(div()).run(node);		}};}

	
	/**
	 * Operator :
	 * To allow JSON like data definition
	 * a:b => kv(a,b) => {a,b}
	 */
	static Pointer kv() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChildren().get(0);
					Node n2 = node.getChildren().get(1);
					return node.setList().addChild(n1).addChild(n2);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}

	/**
	 * Operator %
	 * Requires two Integers (or lists)
	 */
	static Pointer mod() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChildren().get(0);
					Node n2 = node.getChildren().get(1);
					
					// Valid types
					if (n1.isInteger() && n2.isInteger())
						return node.copy(new Node().setInteger(n1.getInteger().mod(n2.getInteger())));
					
					// Invalid types
					if (n1.isFloat() || n2.isFloat()
					||  n1.isBoolean() || n2.isBoolean()
					||  n1.isString() || n2.isString())
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
					// Specials
					if (n1.isVariable() || n2.isVariable())	return node;
					if (n1.isPointer() || n2.isPointer())	return node;
					
					// Lists
					return node.copy(recursive2(n1, n2, this));
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
}

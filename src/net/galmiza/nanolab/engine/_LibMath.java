package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



class _LibMath extends _LibTools {


	// Classes to redefine trigonometric function so that they can be passed as paramters
	// Trigo
	static class Cos extends DDFnc {	Cos(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.cos(d);	}}
	static class Sin extends DDFnc {	Sin(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.sin(d);	}}
	static class Tan extends DDFnc {	Tan(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.tan(d);	}}
	static class Cosh extends DDFnc {	Cosh(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.cosh(d);	}}
	static class Sinh extends DDFnc {	Sinh(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.sinh(d);	}}
	static class Tanh extends DDFnc {	Tanh(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.tanh(d);	}}
	static class Acos extends DDFnc {	Acos(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.acos(d);	}}
	static class Asin extends DDFnc {	Asin(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.asin(d);	}}
	static class Atan extends DDFnc {	Atan(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.atan(d);	}}
	static class Atan2 extends DD2Fnc {	Atan2(String n,Pointer f) {super(n,f);}		double run(double d1, double d2) throws Exception {	return Math.atan2(d1,d2);	}}
	
	
	// Other from Math
	static class Sqrt extends DDFnc {	Sqrt(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.sqrt(d);	}}
	static class Cbrt extends DDFnc {	Cbrt(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.cbrt(d);	}}
	static class Ceil extends DDFnc {	Ceil(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.ceil(d);	}}
	static class Exp extends DDFnc 	{	Exp(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.exp(d);	}}
	static class Exp1m extends DDFnc {	Exp1m(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.expm1(d);	}}
	static class Floor extends DDFnc {	Floor(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.floor(d);	}}
	static class Log extends DDFnc 	{	Log(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.log(d);	}}
	static class Log10 extends DDFnc	{	Log10(String n,Pointer f) {super(n,f);}	double run(double d) throws Exception {	return Math.log10(d);	}}
	static class Log1p extends DDFnc	{	Log1p(String n,Pointer f) {super(n,f);}	double run(double d) throws Exception {	return Math.log1p(d);	}}
	static class ToDegrees extends DDFnc {	ToDegrees(String n,Pointer f) {super(n,f);}	double run(double d) throws Exception {	return Math.toDegrees(d);	}}
	static class ToRadioans extends DDFnc {ToRadioans(String n,Pointer f) {super(n,f);}	double run(double d) throws Exception {	return Math.toRadians(d);	}}
	static class Abs extends DDFnc 	{	Abs(String n,Pointer f) {super(n,f);}		double run(double d) throws Exception {	return Math.abs(d);	}}
	static class Pow extends DD2Fnc {	Pow(String n,Pointer f) {super(n,f);}		double run(double d1, double d2) throws Exception {	return Math.pow(d1,d2);	}}
	

	// Description of behavior for node functions
	static Pointer cos() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Cos("cos",this));	}};	}
	static Pointer sin() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Sin("sin",this));	}};	}
	static Pointer tan() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Tan("tan",this));	}};	}
	static Pointer cosh() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Cosh("cosh",this));	}};	}
	static Pointer sinh() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Sinh("sinh",this));	}};	}
	static Pointer tanh() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Tanh("tanh",this));	}};	}
	static Pointer acos() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Acos("acos",this));	}};	}
	static Pointer asin() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Asin("asin",this));	}};	}
	static Pointer atan() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Atan("atan",this));	}};	}
	static Pointer atan2() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Atan2("atan2",this));	}};	}
	static Pointer sqrt() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Sqrt("sqrt",this));	}};	}
	static Pointer cbrt() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Cbrt("cbrt",this));	}};	}
	static Pointer ceil() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Ceil("ceil",this));	}};	}
	static Pointer exp() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Exp("exp",this));	}};	}
	static Pointer expm1() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Exp1m("expm1",this));	}};	}
	static Pointer floor() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Floor("floor",this));	}};	}
	static Pointer log() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Log("log",this));	}};	}
	static Pointer log10() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Log10("log10",this));	}};	}
	static Pointer log1p() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Log1p("log1p",this));	}};	}
	static Pointer toDegrees() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new ToDegrees("toDegrees",this));	}};	}
	static Pointer toRadians() {return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new ToRadioans("toRadians",this));	}};	}
	static Pointer abs() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Abs("abs",this));	}};	}
	static Pointer pow() {	return new Pointer() {	public Node run(Node node) throws Exception {	return processNode(node, new Pow("pow",this));	}};	}

}

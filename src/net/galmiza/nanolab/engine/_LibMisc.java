package net.galmiza.nanolab.engine;


import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import net.galmiza.nanolab.engine.Function.Pointer;

/**
 *
 *
 */
class _LibMisc extends _LibTools {
	
	/**
	 * Other
	 */
	static Pointer gcd() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger())
						return node.setInteger(n1.getInteger().gcd(n2.getInteger()));
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer factorial() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isInteger()) {
						BigInteger mul = BigInteger.valueOf(1);
						for (int i=2; i<=child.getInteger().intValue(); i++)
							mul = mul.multiply(new BigInteger(Integer.toString(i)));
						return node.setInteger(mul);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer range() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				int c = node.getChildrenCount();
				if (c==1) {
					Node n = node.getChild(0);
					if (n.isInteger()) {
						node.setList();
						for (int i=0; i<n.getInteger().intValue(); i++)
							node.addChild(new Node(i));
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else if (c==2) {
					Node start = node.getChild(0);
					Node stop = node.getChild(1);
					if (start.isInteger() && stop.isInteger()) {
						node.setList();
						for (int i=start.getInteger().intValue(); i<stop.getInteger().intValue(); i++)
							node.addChild(new Node(i));
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else if (c==3) {
					double start = node.getChild(0).getDoubleValue();
					double stop = node.getChild(1).getDoubleValue();
					double step = node.getChild(2).getDoubleValue();
					node.setList();
					if (step>0)	for (double d=start; d<stop; d+=step)	node.addChild(new Node(d));
					else		for (double d=start; d>stop; d+=step)	node.addChild(new Node(d));
					return node;
				} else
					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_COUNT);
			}
		};
	}
	static Pointer rand() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				int c = node.getChildrenCount();
				if (c==0) {
					return node.setFloat(Math.random());
				} else if (c==1) {
					Node n = node.getChild(0);
					if (n.isInteger()) {
						node.setList();
						for (int i=0; i<n.getInteger().intValue(); i++)
							node.addChild(new Node(Math.random()));
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else if (c==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger()) {
						node.setList();
						for (int i=0; i<n1.getInteger().intValue(); i++) {
							Node row = new Node().setList();
							for (int j=0; j<n2.getInteger().intValue(); j++)	row.addChild(new Node(Math.random()));
							node.addChild(row.copy());
						}
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_COUNT);
			}
		};
	}
	
	
	static Pointer time() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==0) {
					return node.setInteger(new BigInteger(String.valueOf(new Date().getTime())));
				} else if (node.getChildrenCount()==1) {
					Node child = node.getChild(0); 
					if (child.isString()) {
						SimpleDateFormat sdf = new SimpleDateFormat(child.getString(), Locale.US);
						return node.setString(sdf.format(new Date()));
					} else
						return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 0));
			}
		};
	}

	
	/**
	 * Print
	 */
	static Pointer print() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					parser.runNode(node.getChild(0));
					System.out.println(node.getChild(0).toString());
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}


}

package net.galmiza.nanolab.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class VariableManager {
	
	// Attributes
	List<Variable> variables;
	int scopeLevel = 0; // reflects the depth of execution (++/-- when entering/exiting user function)
	
	// Constructor
	VariableManager() {
		variables = new ArrayList<Variable>();
	}
	
	// Add
	void add(String name, Node value, int flag) throws Exception {
		remove(name, scopeLevel); // throws exception is not permitted
		variables.add(new Variable(name, value, flag, scopeLevel));
	}

	// Get
	// Returns the variable with the name and highest scope (which is created last)
	// So the loop is from bottom to up (using an iterator)
	Variable getVariable(String name) {
		ListIterator<Variable> li = variables.listIterator(variables.size());
		while(li.hasPrevious()) {
			Variable v = li.previous();
			if (v.name.equals(name))
				return v;
		}
		return null;
	}
	Variable getVariable(String name, int scope) {
		for (Variable v : variables)
			if (v.name.equals(name) && v.scope==scope)
				return v;
		return null;
	}
	Node getValue(String name) {
		Variable v = getVariable(name);
		if (v != null)	return v.value;
		return null;
	}
	
	// Delete
	void remove(String name, int scope) throws Exception {

		// Prevent from using I as a name
		if (name.equals("I"))
			throw new Exception(Error.VARIABLES_RESERVED_I_COMPLEX);
		
		// Prevent from modify built-in
		Variable v = getVariable(name);
		if (v!=null)
			if ((v.flag & Variable.TYPE_BUILT_IN) != 0) 
				throw new Exception(Error.VARIABLES_PROTECTED_BUILT_IN);
		
		// Else delete variable at given scope
		v = getVariable(name, scope);
		if (v!=null)
			variables.remove(getVariable(name));
	}
	
	// Delete all from a scope
	void removeAll(int scope) {
		Iterator<Variable> i = variables.iterator();
		while (i.hasNext()) {
			Variable v = i.next();
			if (v.scope==scope) {
				i.remove();
			}
		}
	}
	
	
}

package net.galmiza.nanolab.engine;


import java.math.BigDecimal;

import net.galmiza.nanolab.engine.Function.Pointer;

public class _LibComparison extends _LibTools {

	/**
	 * Comparators
	 * 
	 * Manages:
	 *  integer vs integer
	 *  float vs float
	 *  float vs integer
	 *  integer vs float
	 *  string vs string
	 *  
	 * Ignore other combinations
	 */
	public static Pointer gt() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_GT, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	public static Pointer ge() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_GE, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	public static Pointer lt() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_LT, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}	
	public static Pointer le() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_LE, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}		
	public static Pointer eq() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_EQ, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}			
	public static Pointer ne() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					return node.setBoolean(compareNodes(COMPARATOR_NE, node.getChild(0), node.getChild(1)));
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}				

	/**
	 * Return the boolean corresponding to the comparison of two nodes
	 */
	private static Boolean compareNodes(int type, Node a, Node b) throws Exception {
		int i = -2; // means undef, next lines set i to -1,0 or 1
		if (a.isInteger() && b.isInteger()) 	i = a.getInteger().compareTo(b.getInteger());
		if (a.isFloat() && b.isFloat()) 		i = a.getFloat().compareTo(b.getFloat());
		if (a.isFloat() && b.isInteger()) 		i = a.getFloat().compareTo(new BigDecimal(b.getInteger()));
		if (a.isInteger() && b.isFloat()) 		i = new BigDecimal(a.getInteger()).compareTo(b.getFloat());
		if (a.isString() && b.isString()) 		i = a.getString().compareTo(b.getString());
		if (a.isBoolean() && b.isBoolean()) 	i = a.getBoolean().compareTo(b.getBoolean());
		if (i!=-2)	return compare(type, i);
		else
			throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
	}
	
	/**
	 * Returns the boolean corresponding a compare result and the type of comparison
	 */
	private static final int COMPARATOR_GT = 0;
	private static final int COMPARATOR_GE = 1;
	private static final int COMPARATOR_LT = 2;
	private static final int COMPARATOR_LE = 3;
	private static final int COMPARATOR_EQ = 4;
	private static final int COMPARATOR_NE = 5;
	private static Boolean compare(int type, int i) {
		switch(type) {
		case COMPARATOR_GT:		return i>0?true:false;
		case COMPARATOR_GE:		return i>=0?true:false;
		case COMPARATOR_LT:		return i<0?true:false;
		case COMPARATOR_LE:		return i<=0?true:false;
		case COMPARATOR_EQ:		return i==0?true:false;
		case COMPARATOR_NE:		return i!=0?true:false;
		}
		return false;		
	}
}

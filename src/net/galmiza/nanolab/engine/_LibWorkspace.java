package net.galmiza.nanolab.engine;

import java.io.File;

import net.galmiza.nanolab.engine.Function.Pointer;


/**
 * Serialize variables and scripted functions
 */
class _LibWorkspace extends _LibTools {

	static Pointer save() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0); 
					if (child.isString()) {
						String s = child.getString();
						parser.saveState(new File(s));
						return node.setVariable("Workspace saved to "+s);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	static Pointer load() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0); 
					if (child.isString()) {
						String s = child.getString();
						parser.loadState(new File(s));
						return node.setVariable("Workspace loaded from "+s);
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
}

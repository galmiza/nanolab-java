package net.galmiza.nanolab.engine;

import net.galmiza.nanolab.engine.Function.Pointer;



class _LibBitwise extends _LibTools {

	/**
	 * Bitwise operators
	 */
	static Pointer notb() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node n1 = node.getChild(0);
					if (n1.isInteger())		return node.setInteger(n1.getInteger().not());
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer andb() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().and(n2.getInteger()));
					else									throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer orb() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().or(n2.getInteger()));
					else									throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer shl() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().shiftLeft(n2.getInteger().intValue()));
					else									throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer shr() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					if (n1.isInteger() && n2.isInteger())	return node.setInteger(n1.getInteger().shiftRight(n2.getInteger().intValue()));
					else									throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}


}

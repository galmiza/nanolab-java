package net.galmiza.nanolab.engine;


/**
 * Load parser with:
 * - Operators
 * - Variables
 * - Native functions
 * - Scripted functions
 */
public class Init {

	public static void loadOperators(Parser parser) {
		
		int l = Operator.LEFT_OPERAND;
		int r = Operator.RIGHT_OPERAND;
		parser.addOperator("++", "inc", l);
		parser.addOperator("--", "dec", l);

		parser.addOperator("~", "notb", r);
		parser.addOperator("&", "andb", l+r);
		parser.addOperator("|", "orb", l+r);
		parser.addOperator("<<", "shl", l+r);
		parser.addOperator(">>", "shr", l+r);
		
		parser.addOperator(":", "kv", l+r); // To allow JSON like data definition, a:b => kv(a,b) => {a,b}
		parser.addOperator("!", "factorial", l); // WARNING, not logical negation
		parser.addOperator("°", "toRadian", l);
		parser.addOperator("√", "sqrt", r);
		
		parser.addOperator("^", "pow", l+r); // WARNING, not bitwise xor
		parser.addOperator("*", "mul", l+r);
		parser.addOperator("/", "div", l+r);
		parser.addOperator("%", "mod", l+r);
		parser.addOperator("-", "sub", l+r);
		parser.addOperator("+", "add", l+r);
		
		parser.addOperator("+=", "addi", l+r);
		parser.addOperator("-=", "subi", l+r);
		parser.addOperator("*=", "muli", l+r);
		parser.addOperator("/=", "divi", l+r);
		
		parser.addOperator("=", "set", l+r);
		
		parser.addOperator(">", "gt",  l+r);
		parser.addOperator(">=", "ge", l+r);
		parser.addOperator("<", "lt",  l+r);
		parser.addOperator("<=", "le", l+r);
		parser.addOperator("==", "eq", l+r);
		parser.addOperator("!=", "ne", l+r);
		parser.addOperator("<>", "ne", l+r);
		parser.addOperator("&&", "and", l+r);
		parser.addOperator("||", "or", l+r);
	}

	public static void loadVariables(Parser parser) throws Exception {

		parser.addVariable("Pi", new Node(3.14159265358979323846264338327950288419716939937510582097), Variable.TYPE_BUILT_IN);
		parser.addVariable("e",  new Node(2.7182818284590452353602874713527), Variable.TYPE_BUILT_IN);
		parser.addVariable("true",  new Node(true), Variable.TYPE_BUILT_IN);
		parser.addVariable("false",  new Node(false), Variable.TYPE_BUILT_IN);	
		parser.addVariable("null",  new Node().setVoid(), Variable.TYPE_BUILT_IN);
	}

	public static void loadNativeFunctions(Parser parser) throws Exception {
		
		// Settings
		int b = Function.TYPE_BUILT_IN;
		parser.addFunction("setDecimalPrintPrecision", _LibTools.setDecimalPrintPrecision(), b);
		parser.addFunction("setDecimalExecutionPrecision", _LibTools.setDecimalPrintPrecision(), b);
		
		// Callback / run / execute
		parser.addFunction("call", _LibDefinition.call(), b);
		parser.addFunction("run", _LibDefinition.run(), b);
		parser.addFunction("execute", _LibDefinition.execute(), b);
		
		// Operators
		parser.addFunction("add", _LibArithmetic.add(), b);
		parser.addFunction("sub", _LibArithmetic.sub(), b);
		parser.addFunction("mul", _LibArithmetic.mul(), b);
		parser.addFunction("div", _LibArithmetic.div(), b);
		parser.addFunction("mod", _LibArithmetic.mod(), b);
		parser.addFunction("kv",  _LibArithmetic.kv(), b);
		parser.addFunction("pow", _LibMath.pow(), b);
		parser.addFunction("inc", _LibArithmetic.inc(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("dec",  _LibArithmetic.dec(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("addi",  _LibArithmetic.addi(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("subi",  _LibArithmetic.subi(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("muli",  _LibArithmetic.muli(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("divi",  _LibArithmetic.divi(), b + Function.BLOCK_RECURSIVE_EXECUTION);

		// Comparators
		parser.addFunction("gt", _LibComparison.gt(), b);
		parser.addFunction("ge", _LibComparison.ge(), b);
		parser.addFunction("lt", _LibComparison.lt(), b);
		parser.addFunction("le", _LibComparison.le(), b);
		parser.addFunction("eq", _LibComparison.eq(), b);
		parser.addFunction("ne", _LibComparison.ne(), b);
		
		// Logical operators
		parser.addFunction("and", _LibLogical.and(), b);
		parser.addFunction("or", _LibLogical.or(), b);
		parser.addFunction("xor", _LibLogical.xor(), b);
		
		// Bitwise operators
		parser.addFunction("notb", _LibBitwise.notb(), b);
		parser.addFunction("andb", _LibBitwise.andb(), b);
		parser.addFunction("orb", _LibBitwise.orb(), b);
		parser.addFunction("shl", _LibBitwise.shl(), b);
		parser.addFunction("shr", _LibBitwise.shr(), b);
		
		// Algorithm
		parser.addFunction("if", _LibAlgorithm.if_(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("while", _LibAlgorithm.while_(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("for", _LibAlgorithm.for_(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("return", _LibAlgorithm.return_(), b); // Do not set algorithm flag
		parser.addFunction("break", _LibAlgorithm.break_(), b); // Do not set algorithm flag
		parser.addFunction("continue", _LibAlgorithm.continue_(), b); // Do not set algorithm flag
		
		// Assignments
		parser.addFunction("set", _LibDefinition.set(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("unset", _LibDefinition.unset(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("getType", _LibDefinition.getType(), b);
		parser.addFunction("getChild", _LibChildren.getChild(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("setChild", _LibChildren.setChild(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("rmChild", _LibChildren.rmChild(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("addChild", _LibChildren.addChild(), b + Function.BLOCK_RECURSIVE_EXECUTION);
		parser.addFunction("insChild", _LibChildren.insChild(), b + Function.BLOCK_RECURSIVE_EXECUTION);

		
		// Aggregation
		parser.addFunction("count", _LibAggregation.count(), b);
		
		// Strings
		parser.addFunction("charAt", _LibString.charAt(), b);
		parser.addFunction("indexOf", _LibString.indexOf(), b);
		parser.addFunction("split", _LibString.split(), b);
		parser.addFunction("substring", _LibString.substring(), b);
		parser.addFunction("replace", _LibString.replace(), b);
		
		// Trigo
		parser.addFunction("cos", _LibMath.cos(), b);
		parser.addFunction("sin", _LibMath.sin(), b);
		parser.addFunction("tan", _LibMath.tan(), b);
		parser.addFunction("cosh", _LibMath.cosh(), b);
		parser.addFunction("sinh", _LibMath.sinh(), b);
		parser.addFunction("tanh", _LibMath.tanh(), b);
		parser.addFunction("acos", _LibMath.acos(), b);
		parser.addFunction("asin", _LibMath.asin(), b);
		parser.addFunction("atan", _LibMath.atan(), b);
		parser.addFunction("atan2", _LibMath.atan2(), b);

		// Math
		parser.addFunction("sqrt", _LibMath.sqrt(), b);
		parser.addFunction("cbrt", _LibMath.cbrt(), b);
		parser.addFunction("ceil", _LibMath.ceil(), b);
		parser.addFunction("exp", _LibMath.exp(), b);
		parser.addFunction("expm1", _LibMath.expm1(), b);
		parser.addFunction("floor", _LibMath.floor(), b);
		parser.addFunction("log", _LibMath.log(), b);
		parser.addFunction("log10", _LibMath.log10(), b);
		parser.addFunction("log1p", _LibMath.log1p(), b);
		parser.addFunction("toDegrees", _LibMath.toDegrees(), b);
		parser.addFunction("toRadians", _LibMath.toRadians(), b);
		parser.addFunction("abs", _LibMath.abs(), b);
		parser.addFunction("factorial", _LibMisc.factorial(), b);
		parser.addFunction("gcd", _LibMisc.gcd(), b);

		// Matrix
		parser.addFunction("zeros", _LibMatrix.zeros(), b);
		parser.addFunction("ones", _LibMatrix.ones(), b);
		parser.addFunction("eye", _LibMatrix.eye(), b);
		parser.addFunction("inv", _LibMatrix.inv(), b);
		parser.addFunction("det", _LibMatrix.det(), b);

		// Conversions
		parser.addFunction("int", _LibConversion.int_(), b);
		parser.addFunction("string", _LibConversion.string_(), b);
		parser.addFunction("float", _LibConversion.float_(), b);
		parser.addFunction("isFloat", _LibConversion.isFloat(), b);
		parser.addFunction("isInteger", _LibConversion.isInteger(), b);
		parser.addFunction("isString", _LibConversion.isString(), b);
		parser.addFunction("isList", _LibConversion.isList(), b);
		parser.addFunction("isBoolean", _LibConversion.isBoolean(), b);
		parser.addFunction("isVariable", _LibConversion.isVariable(), b);
		
		// Files
		parser.addFunction("ls", _LibFile.ls(), b);
		parser.addFunction("fileRead", _LibFile.fileRead(), b);
		parser.addFunction("fileWrite", _LibFile.fileWrite(), b);
		parser.addFunction("fileDelete", _LibFile.fileDelete(), b);
		parser.addFunction("fileExists", _LibFile.fileExists(), b);
		
		// Workspace
		parser.addFunction("save", _LibWorkspace.save(), b);
		parser.addFunction("load", _LibWorkspace.load(), b);
		
		// Other
		//parser.addFunction("print", _LibMisc.print(), b);
		parser.addFunction("range", _LibMisc.range(), b);
		parser.addFunction("rand", _LibMisc.rand(), b);
		parser.addFunction("time", _LibMisc.time(), b);
	}

	public static void loadScriptedFunctions(Parser parser) throws Exception {

		//parser.runExpression("help(s)=man(s)"); // alias
		parser.runExpression("length(s)=count(s)"); // alias
		//parser.runExpression("range(args)={l={},if(count(args)==1,l=range({0,args[0],1}),if(count(args)==2,l=range({args[0],args[1],1}),if(count(args)==3,{{start,stop,step}=args,while(start<=stop,{l[]=start,start+=step})},false))),return(l)}");
		parser.runExpression("first(l)=l[0]");
		parser.runExpression("last(l)=l[count(l)-1]");
		parser.runExpression("sum(l)={n=count(l),if(n>0,if(n>1,{r=l[0],for(i,1,n-1,r+=l[i]),return(r)},return(l[0])),return(0))}");
		parser.runExpression("avg(l)=sum(l)/count(l)");
		parser.runExpression("std(l)=sqrt(sum((l-avg(l))^2)/count(l))");
		parser.runExpression("min(l)={r=l[0],for(i,l,if(r>i,r=i,0)),return(r)}");
		parser.runExpression("max(l)={r=l[0],for(i,l,if(r<i,r=i,0)),return(r)}");
		parser.runExpression("cross(u,v)={u[1]*v[2]-u[2]*v[1],u[2]*v[0]-u[0]*v[2],u[0]*v[1]-u[1]*v[0]}"); //u={2,3,4} v={5,6,7}
		parser.runExpression("dot(u,v)={r=0,for(i,0,count(u)-1,r+=u[i]*v[i]),return(r)}");
		parser.runExpression("norm2(u)={r=0,for(i,0,count(u)-1,r+=u[i]*u[i]),return(r)}");
		parser.runExpression("norm(u)=sqrt(norm2(u))");
		parser.runExpression("reverse(s)={r=\"\",for(c,s,r=c+r),return(r)}");
		parser.runExpression("join(s,l)=if(count(l)!=0,{r=l[0],for(i,1,count(l)-1,r=r+s+l[i]),return(r)},\"\")");
		parser.runExpression("nPr(n,p)=int(n!/(n-p)!)");
		parser.runExpression("nCr(n,p)=int(nPr(n,p)/p!)");
		parser.runExpression("toRadian(x)=Pi*x/180");
		parser.runExpression("toDegree(x)=180*x/Pi");
		parser.runExpression("decToBin(d)={k=\"01\",r=\"\",while(d>0,{r=k[d%2]+r,d=int(d/2)}),return(r)}");
		parser.runExpression("decToHex(d)={k=\"0123456789ABCDEF\",r=\"\",while(d>0,{r=k[d%16]+r,d=int(d/16)}),return(r)}");
		parser.runExpression("binToDec(b)={b=reverse(b),k=\"01\",r=0,j=1,for(i,b,{r+=j*indexOf(i,k),j*=2}),return(r)}");
		parser.runExpression("hexToDec(h)={h=reverse(h),k=\"0123456789ABCDEF\",r=0,j=1,for(i,h,{r+=j*indexOf(i,k),j*=16}),return(r)}");
		parser.runExpression("binToHex(b)=decToHex(binToDec(b))");
		parser.runExpression("hexToBin(h)=decToBin(hexToDec(h))");
		parser.runExpression("isSquare(m)={l=size(m),if(count(l)==2,if(l[0]==l[1],return(true),return(false)),return(false))}");
		parser.runExpression("trace(m)=if(isSquare(m),{r=0,for(i,0,count(m)-1,r+=m[i][i]),return(r)},undef)");
		parser.runExpression("size(m)={l={},while(isList(m),{l[]=count(m),m=m[0]}),return(l)}");
		parser.runExpression("transpose(m)={l=size(m),d1=l[1],d2=l[0],l=zeros(d1,d2),for(i,0,d1-1,for(j,0,d2-1,l[i][j]=m[j][i])),return(l)}");
		
		// Set them all as scripted and built-in to prevent modifications
		for (Function f : parser.functionManager.functions)
			if ((f.flag & Function.TYPE_BUILT_IN)==0)
				f.flag |= Function.TYPE_SCRIPTED + Function.TYPE_BUILT_IN;
	}

}

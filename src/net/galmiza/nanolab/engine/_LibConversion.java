package net.galmiza.nanolab.engine;


import java.math.BigDecimal;
import java.math.BigInteger;

import net.galmiza.nanolab.engine.Function.Pointer;

class _LibConversion extends _LibTools {

	/**
	 * Type convertions
	 */
	// float -> int, string -> int, boolean -> int
	static Pointer int_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isInteger())		node.setInteger(child.getInteger());
					else if (child.isFloat())	node.setInteger(child.getFloat().toBigInteger());
					else if (child.isString())	node.setInteger(new BigInteger(child.getString()));
					else if (child.isBoolean())	node.setInteger(child.getBoolean()?BigInteger.valueOf(1):BigInteger.valueOf(0));
					else if (child.isList()) 	node.copy(recursive1(child, this));
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}

	// string -> float, int -> float, boolean -> float
	static Pointer float_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isFloat())		node.setFloat(child.getFloat());
					else if (child.isInteger())	node.setFloat(new BigDecimal(child.getInteger()));
					else if (child.isString())	node.setFloat(new BigDecimal(child.getString()));
					else if (child.isBoolean())	node.setFloat(child.getBoolean()?BigDecimal.valueOf(1):BigDecimal.valueOf(0));
					else if (child.isList()) 	node.copy(recursive1(child, this));
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}	
	
	// float -> string, int -> string
	static Pointer string_() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = node.getChild(0);
					if (child.isString())		node.setString(child.getString());
					else if (child.isFloat())	node.setString(child.getFloat().setScale(parser.getDecimalPrintPrecision(), parser.getRoundingMode()).stripTrailingZeros().toString());
					else if (child.isInteger())	node.setString(child.getInteger().toString());
					else if (child.isBoolean())	node.setString(child.getBoolean()?"true":"false");
					else if (child.isList()) 	node.copy(recursive1(child, this));
					return node;
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a float
	static Pointer isFloat() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isFloat());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a integer
	static Pointer isInteger() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isInteger());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a boolean
	static Pointer isBoolean() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isBoolean());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a string
	static Pointer isString() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isString());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a variable
	public static Pointer isVariable() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isVariable());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	// return true if node represents a list
	static Pointer isList() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1)	return node.setBoolean(node.getChild(0).isList());
				else							throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
}

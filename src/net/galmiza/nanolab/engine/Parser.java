package net.galmiza.nanolab.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import net.galmiza.nanolab.engine.Function.Pointer;


/**
 * Main class
 * Parses string to Node structure 
 * Initiate Node structure execution
 * Manage settings (float precision)
 */
public class Parser {
	
	// Managers
	VariableManager variableManager;
	FunctionManager functionManager;
	OperatorManager operatorManager;
	
	// Settings
	private int decimalExecutionPrecision = 100;
	private int decimalPrintPrecision = 6;
	private boolean forceEngineeringNotation = false;
	private RoundingMode roundingMode = RoundingMode.HALF_UP;
	
	// Constructor
	public Parser() throws Exception {
		
		// Init core objects managers
		operatorManager = new OperatorManager();
		variableManager = new VariableManager();		
		functionManager = new FunctionManager();
		
		// Share parser with Node
		Node.parser = this;
	}
	
	
	/**
	 * Settings
	 */
	public void setDecimalExecutionPrecision(int p) 	{	decimalExecutionPrecision = p;	}
	public void setDecimalPrintPrecision(int p) 		{	decimalPrintPrecision = p;	}
	public void setEngineeringNotation(boolean b) 		{	forceEngineeringNotation = b;	}
	public void setRoundingMode(RoundingMode rm) 		{	roundingMode = rm;	}
	public int getDecimalExecutionPrecision() 			{	return decimalExecutionPrecision;	}
	public int getDecimalPrintPrecision() 				{	return decimalPrintPrecision;	}
	public boolean getEngineeringNotation() 			{	return forceEngineeringNotation;	}
	public RoundingMode getRoundingMode() 				{	return roundingMode;	}
	
	
	/**
	 * Operator management
	 */
	public void addOperator(String symbol, String name, int flag) {
		operatorManager.add(symbol, name, flag);	}
	
	
	/**
	 * Variable management
	 */
	public List<Variable> getVariables() {	return variableManager.variables;	}
	public Node getVariableFromName(String name) {
		return variableManager.getValue(name);	}
	public void addVariable(String name, Node value, int flag) throws Exception {
		variableManager.add(name, value, flag);	}
	public void removeVariable(String name, int scope) throws Exception {
		variableManager.remove(name, scope);	}
	public void removeVariable(String name) throws Exception {
			for (int scope=0; scope<100; scope++) variableManager.remove(name, scope);	} // TODO change brute force
	public void removeAllVariables(int scope) {	variableManager.removeAll(scope);	}
	public int getVariableScope() {	return variableManager.scopeLevel;	}
	public void increaseVariableScope() {	variableManager.scopeLevel++;	}
	public void decreaseVariableScope() {	variableManager.scopeLevel--;	}
	
	
	/**
	 * Function management
	 */
	public List<Function> getFunctions() {	return functionManager.functions;	}
	public List<Function> getScriptedFunctions() {	return functionManager.getScriptedFunction();	}
	public void addFunction(String name, Pointer pointer, int flag) throws Exception {
		functionManager.add(name, pointer, flag);	}
	public void addUserFunction(String name, Node target, Node expression, int flag) throws Exception {
		functionManager.add(name, target, expression, flag);	}
	public void removeFunction(String name) throws Exception {
		functionManager.remove(name);	}
	public Pointer getFunctionPointer(String name) throws Exception {
		return functionManager.getPointer(name);	}
	public String getFunctionName(Pointer f) {
		return functionManager.getName(f);
	}
	
	/**
	 * Process a code (code=several expression separated by ;)
	 */
	public void runCode(String code) throws Exception {
		code = removeComments(code);
		code = removeSpaces(code);
		code = removeLineReturns(code);
		
		// Remove consecutive ;
		code = code.replaceAll(";+", ";");
		
		// Extract expressions from code
		List<String> expressions = new ArrayList<String>();
		int i=0;
		int pos=0;
		while (i<code.length()-1) {
			if (isPartOfString(code, i++))	continue;
			if (code.charAt(i-1)==';') {
				expressions.add(code.substring(pos,i-1));
				pos = i;
			}
		}
		expressions.add(code.substring(pos,i));
		
		// Build and run nodes
		for (String s : expressions)
			runExpression(s);
	}
	
	/**
	 * Process in one call all steps to get execution result from a string to string
	 */
	public String runExpression(String s) throws Exception {
    	s = processExpression(s);
    	Node node = expandNode(s);
    	_LibAlgorithm.return_ = null; // reset return value
    	runNode(node);
    	return node.toString();
	}
		

	

	
	/**
	 * Execute functions within the node hierarchiy
	 * assign = true to only process functions that need unresolved variables (set, unset, getChild, setChild, )
	 * Does NOT generate exceptions
	 */
	public Node runNode(final Node node) throws Exception {
		
		// Children of a list has to be resolved before the node
		if (node.isList()) {
			for (Node n : node.getChildren())
				runNode(n);
			node.run();
		}
		
		// Run pointers
		if (node.isPointer()) {
			
			// Check if recursive run is needed
			boolean recursive = false;
			Function f = functionManager.getFunction(node.getString());
			if (f!=null) {
				node.setPointer(f.pointer);
				if ((f.flag & Function.BLOCK_RECURSIVE_EXECUTION)==0) // do not run assignments nor algorithms to avoid user defined functions expression assignments to be run (f(a)={i=3, ...} would be defined {3,...})
					recursive = true;
			} else	recursive = true;
			
			// Run children first if needed
			if (recursive)
				for (Node n : node.getChildren())
					runNode(n);
			
			node.run();
		}
		
		// Replace variables by their value is available
		if (node.isVariable()) {
			Node n = variableManager.getValue(node.getString());
			if (n != null) {
				node.copy(n); // make a copy to avoid having runNode overwritting the variable variables
				runNode(node); // run variable so that when a=b b=c then a returns c (and not only b)
			}
		}
		return node;		
	}
	
	
	/**
	 * Build node hierarchy from expression
	 * Generates PARSING exceptions
	 */
	private Node expandNode(String s) throws Exception {
		s = removeExtraParenthesis(s);
		Node out = new Node().setList();
		
		// Exit if 0 length
		if (s.length()==0) return out;
		int pos = 0;
		
		// Process char by char
		char c = s.charAt(pos);
		if (isCharFigure(c)) {
			out.setFloat(new BigDecimal(s.substring(pos)));	// Double?
			try { // Integer? (try AFTER double)
				out.setInteger(new BigInteger(s.substring(pos)));	}
			catch (NumberFormatException e) {} // failed to convert a valid float to integer, never mind, just keep the float
		}
		else if (isCharCharacter(c)) {
			// Function 'func(...)' OR variable 'var'
			
			// Find end of valid char for variable/function name or (
			int start = pos;
			while (pos<s.length()) {
				char o = s.charAt(pos);
				if (isPartOfString(s, pos++)) continue;
				if (!(isCharCharacter(o) || isCharFigure(o))) break;
				if (o=='(') break;
			}
			
			// Function found ?
			if (s.charAt(pos-1)=='(') {
				
				String pointerName = s.substring(start,pos-1); // The corresponding pointer will be calculated at runtime
	
				// Search closing )
				start = pos;
				int n=0, m=0; // ( and { counters
				while (pos<s.length()) {
					char o = s.charAt(pos);
					if (isPartOfString(s, pos++)) continue;
					if (o=='(') n++;
					if (o==')') n--;
					if (o=='{') m++;
					if (o=='}') m--;
					if (n==-1 && m==0) break;
				}
				
				// Control that no char are following
				if (pos!=s.length())										
					throw new Exception(Error.PARSING_INVALID_TRAILING_CHAR);
				if (pos==s.length() && s.charAt(pos-1)!=')')
					throw new Exception(Error.PARSING_MISSING_CLOSING_PARENTHESIS);
				
				// Get data between (...)
				String inner = s.substring(start,pos-1);
				if (inner.length()>0) {
					Node tmp = new Node();
					tmp.setList();
					computeChildren(tmp, inner);
					out.copy(tmp);
				}
				
				out.setPointer(pointerName);
				
			// Variable found
			} else if (isCharCharacter(s.charAt(pos-1)) || isCharFigure(s.charAt(pos-1))) { 
				String varName = s.substring(start,pos);
				out.setVariable(varName);
				
			} else
				throw new Exception(Error.PARSING_INVALID_EXPRESSION);
				
		}
		else if (c=='{') {
			
			// Search closing }
			if (s.length()==1)	throw new Exception(Error.PARSING_MISSING_CLOSING_BRACE);
			int n=0, m=0; // ( and { counters
			pos++;
			int start = pos;
			while (pos<s.length()) {
				char o = s.charAt(pos);
				if (isPartOfString(s, pos++)) continue;
				if (o=='(') n++;
				if (o==')') n--;
				if (o=='{') m++;
				if (o=='}') m--;
				if (o=='}' && n==0 && m==-1) break;
			}
			computeChildren(out, s.substring(start,pos-1));

			// Control that no char are following
			if (pos!=s.length())
				throw new Exception(Error.PARSING_INVALID_TRAILING_CHAR);
			if (pos==s.length() && s.charAt(pos-1)!='}')
				throw new Exception(Error.PARSING_MISSING_CLOSING_BRACE);
		}
		else if (c=='"') {
			
			// Invalid expression if expression is just '"'
			if (s.length()==1)			throw new Exception(Error.PARSING_MISSING_CLOSING_QUOTE);
			
			// Search for ending "
			int i = s.substring(pos+1).indexOf('"');
			if (i==-1)					throw new Exception(Error.PARSING_MISSING_CLOSING_QUOTE);
			out.setString(s.substring(pos+1, pos+1+i));
			
			// Invalid end of expression if right " is not the last char
			if (pos+i+2!=s.length())	throw new Exception(Error.PARSING_MISSING_CLOSING_QUOTE);
		}
		else
			throw new Exception(Error.PARSING_INVALID_EXPRESSION);
		
		return out;
	}
	
	/**
	 * Add elements to the given node from the expression
	 * This function is called recursively
	 * The expression does not contain boundaries (eg. 1,3,{0,{1,pow(0.4,2)}},"hi")
	 */
	private void computeChildren(final Node node, String s) throws Exception {

		// Empty list?
		if (s.length()==0) {
			node.setList();
			return;
		}
		
		// Split , and avoid inner {} or ()
		s = s+","; // add extrem , to avoid having to process last item
		int start = 0;
		int pos = 0;
		int n=0, m=0; // ( and { counters
		while (pos<s.length()) {
			char o = s.charAt(pos);
			if (isPartOfString(s, pos++)) continue;
			if (o=='(') n++;
			if (o==')') n--;
			if (o=='{') m++;
			if (o=='}') m--;
			if (o==',' && n==0 && m==0) {
				node.addChild(expandNode(s.substring(start,pos-1)));
				start = pos;
			}
		}
	}
	
	/**
	 * Basic char comparisons
	 */
	private boolean isCharFigure(char c) {		return c>='0' && c<='9';	}
	private boolean isCharCharacter(char c) {	return (c == '.' || c == '_' || (c <= 'Z' && c >= 'A') || (c <= 'z' && c >= 'a'));	} // Note . is allowed as part of variable/function name
	
	
	/**
	 * Process all
	 */
	public String processExpression(String s) throws Exception {

		s = removeComments(s);
		s = removeSpaces(s);
		s = removeLineReturns(s);
		s = processFloats(s);
		s = processSpecialCharacters(s);
		s = processBrakets(s);
		s = processOperators(s);
		s = processChildrenAssigments(s);
		
		return s;
	}
	
	/**
	 * Replace scientific notations like -3.04e-5 by -3.04*10^(-5)
	 *  to avoid wrong processing of operators
	 */
	private String processFloats(String s) {
		s = s.replaceAll("([-+]?[0-9]*\\.?[0-9]+)[eE]+([-][0-9]?)", "$1*10^($2)"); // keep - sign
		s = s.replaceAll("([-+]?[0-9]*\\.?[0-9]+)[eE]+[+]?([0-9]?)", "$1*10^($2)"); // ignore + sign
		return s;
	}

	/**
	 * Replace symbols by their values (example π=>Pi)
	 */
	private String processSpecialCharacters(String s) {
		for (int i=0; i<s.length(); i++) {
			if (isPartOfString(s,i)) continue;
			char c = s.charAt(i);
			if (c=='π') s = s.substring(0,i)+"Pi"+s.substring(i+1);
			if (c=='×') s = s.substring(0,i)+"*"+s.substring(i+1);
			if (c=='÷') s = s.substring(0,i)+"/"+s.substring(i+1);
		}
		 return s;
	}

	/**
	 * Replaces a[b] by getChild(b,a)
	 * Replaces a[b][c] by getChild({b,c},a) and not getChild(b,getChild(c,a))
	 */
	private String processBrakets(String s) throws Exception {
		
		String old;
		do {
			old = s;
			
			// Search for [
			int i;
			for (i=0; i<s.length(); i++) {
				if (isPartOfString(s, i)) continue;
				if (s.charAt(i) == '[') break;
			}
			if (i==s.length()) return s;
			int pos=i;

			// Get left expression
			String a = getLeftExpression(s, pos);
			
			// Find path in list
			int j = pos+1;
			List<String> indexes = new ArrayList<String>();
			while (true) {
	
				// Search for closing ]
				int n=1; // [] counter
				for (i=j; i<s.length(); i++) {
					if (isPartOfString(s, i)) continue;
					char c = s.charAt(i);
					if (c=='[') n++;
					if (c==']') n--;
					if (c==']' && n==0) break;
				}
				String b = s.substring(j,i);
				indexes.add(b);

				// If next char is [, then we have a x[y][z][...] access
				if (i+1==s.length()) 		break;
				if (i+1>s.length())
					throw new Exception(Error.PARSING_MISSING_CLOSING_BRACKET);
				if (s.charAt(i+1)!='[')		break;
				j = i+2;
			}

			// Build list of indexes
			String path = "";
			for (String o : indexes)	path += o+",";
			path = path.substring(0,path.length()-1); // merge indexes => 0,4,6
			if (indexes.size()>1) path = String.format("{%s}", path); // surround by {} if more than one index

			s = s.substring(0,pos-a.length())+String.format("getChild(%s,%s)",path,a)+s.substring(i+1);
			
		} while (!s.equals(old));
		
		return s;
	}
	
	/**
	 * set(child(i,a),b) => setChild(i,a,b)
	 * set(child({...},a),b) => setChild({...},a,b)
	 * Note: no recursivity needed here because this is initially a variable definition (with =)
	 */
	private String processChildrenAssigments(String s) throws Exception {
		
		String search = "set(getChild(";
		String replace = "setChild(";
		
		// Find string to replace
		int pos;
		pos = s.indexOf(search);
		if (pos==-1) return s;
		if (isPartOfString(s, pos)) return s;
		
		// Replace string
		s = s.substring(0,pos)+replace+s.substring(pos+search.length());
			
		// Remove 1st closing )
		int n=0,m=0; // (){} counter (--, (++
		int i;
		for (i=pos+replace.length(); i<s.length(); i++) {
			char c = s.charAt(i);
			if (isPartOfString(s, i)) continue;
			if (c==')') n++;
			if (c=='}') m++;
			if (c=='(') n--;
			if (c=='{') m--;
			if (n==1 && m==0) break; 
		}
		s = s.substring(0,i)+s.substring(i+1); // remove 
		
		return s;
	}
	
	/**
	 * Change operator symbol by their function name
	 * Respect operator priority
	 * Each operator has a flag to tell if a left and/or right operand are expected
	 */
	private String processOperators(String s) throws Exception {
		
		// Protect extremities with ()
		s = String.format("(%s)", s); 

		// Protect - operators with not left assignement by adding a 0
		String old;
		do {
			old = s;
			s = s.replaceAll("(.*?\\()\\-(.*)", "$10-$2"); // x(-a => x(0-a
			s = s.replaceAll("\\{\\-(.*)", "{0-$1"); // {-a => {0-a
			s = s.replaceAll(",\\-(.*)", ",0-$1"); // ,-a => ,0-a
		} while (!s.equals(old));
		
		
		// Replace operators symbols by their functions
		// Be careful of characters shared by several operator symbols (eg. = and >=)
		for (Operator o : operatorManager.getOperators()) {
			int l = o.symbol.length();
			
			int i = 0; // char by char scan index
			while (i<s.length()) {
				int pos = i+s.substring(i).indexOf(o.symbol); // Find the symbol of the operator (ignoring what is before the scan index)
				if (pos==i-1) break; // No symbol found, process next operator
				if (isPartOfString(s, pos)) { i++; continue; }
				
				// Check if symbol found is not part of another longer operator symbol
				if (operatorManager.getOperatorFromSymbol(o.symbol+String.valueOf(s.charAt(pos+1))) != null) {	i = pos+l;	continue;	}
				if (operatorManager.getOperatorFromSymbol(String.valueOf(s.charAt(pos-1))+o.symbol) != null) {	i = pos+1;	continue;	}
				
				// Replace symbol by function
				if (o.flag == Operator.LEFT_OPERAND) {
					
					// Get expression on the left and replace by function name
					String left = getLeftExpression(s, pos);
					String f = String.format("%s(%s)", o.name, left); 
					s = s.substring(0,pos-left.length())+f+s.substring(pos+l);
					s = s.replace(left, removeExtraParenthesis(left));
					i = 0; // Reset scan index

				} else if (o.flag == Operator.RIGHT_OPERAND) {
					
					// Get expression on the right and replace by function name
					String right = getRightExpression(s, pos+l-1);
					String f = String.format("%s(%s)", o.name, right); 
					s = s.substring(0,pos)+f+s.substring(pos+right.length()+l);
					s = s.replace(right, removeExtraParenthesis(right));
					i = 0; // Reset scan index

				} else {
					
					// Get expression surrounding the operator
					String left = getLeftExpression(s, pos);
					String right = getRightExpression(s, pos+l-1);
					
					// Replace the operator by its function name
					String f = String.format("%s(%s,%s)", o.name, left, right); 
					s = s.substring(0,pos-left.length())+f+s.substring(pos+right.length()+l);
					s = s.replace(left, removeExtraParenthesis(left));
					s = s.replace(right, removeExtraParenthesis(right));
					i = 0; // Reset scan index
				}
				i++;
			}
		}
		s = removeExtraParenthesis(s);
				
		return s;
	}
	
	/**
	 * Remove spaces and line returns that do not belong to a string
	 */
	private String removeSpaces(String s) {
		for (int i=0; i<s.length(); i++) {
			if (isPartOfString(s, i)) continue;
			if (s.charAt(i)==' ' || s.charAt(i)=='\t') {
				s = s.substring(0,i)+s.substring(i+1);
				i--;
			}
		}
		return s;
	}
	private String removeLineReturns(String s) {
		for (int i=0; i<s.length(); i++) {
			if (isPartOfString(s, i)) continue;
			if (s.charAt(i)=='\n' || s.charAt(i)=='\r') {
				s = s.substring(0,i)+s.substring(i+1);
				i--;
			}
		}
		return s;
	}
	
	
	/**
	 * Remove comments defined:
	 *  -after // on each line
	 *  -between /* and * / (without space)
	 *  Note: comment are ignored if part of a string
	 */
	private String removeComments(String s) {
		String old;
		
		// Remove chars after // on each line
		do {
			old = s;
			for (int i=0; i<s.length()-1; i++) {
				if (isPartOfString(s, i)) continue;
				
				// Search for //
				if (s.charAt(i)=='/' && s.charAt(i+1)=='/') {
					
					// Search for line return \r or \n
					int j;
					for (j=i; j<s.length(); j++)
						if (s.charAt(j)=='\r' || s.charAt(j)=='\n')
							break;
					s = s.substring(0,i)+s.substring(j);
				}
			}
		} while (!s.equals(old));
		
		// Remove char between /**/
		do {
			old = s;
			for (int i=0; i<s.length()-1; i++) {
				if (isPartOfString(s, i)) continue;
				
				// Search for //
				if (s.charAt(i)=='/' && s.charAt(i+1)=='*') {
					
					// Search for line return \r or \n
					int j;
					for (j=i; j<s.length()-1; j++)
						if (s.charAt(j)=='*' && s.charAt(j+1)=='/')
							break;
					s = s.substring(0,i)+s.substring(j+2);
				}
			}
		} while (!s.equals(old));
		
		return s;
	}
	
	/**
	 * Returns the number of occurence on the character in the string
	 */
	private int getCharCount(String s, char c) {
		int n=0;
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i) == c) 
				n++;
		return n;
	}
	
	/**
	 * Removes the surrounding parenthesis if any
	 */
	public String removeExtraParenthesis(String s) {
		if (s.length()<1) return s;
		if (s.charAt(0)=='(' && s.charAt(s.length()-1)==')')
			return removeExtraParenthesis(s.substring(1,s.length()-1));
		return s;
	}
	
	/**
	 * Returns the expression on the right of the char at position pos
	 */
	private String getRightExpression(String s, int pos) throws Exception {
		int n=0,m=0; // (){} counter (++, (--
		int i;
		for (i=pos+1; i<s.length()-1; i++) {
			if (isPartOfString(s, i)) continue;
			char c = s.charAt(i);
			if ((isCharOperator(c) || c==',' || c=='=' || c==')' || c=='}') && n==0 && m==0)
				break;
			if (c=='(') n++;
			if (c=='{') m++;
			if (c==')') n--;
			if (c=='}') m--;
		}
		return s.substring(pos+1,i);
	}
	
	/**
	 * Returns the expression on the left of the char at position pos
	 */
	private String getLeftExpression(String s, int pos) throws Exception {
		int n=0,m=0; // (){} counter (--, (++
		int i;
		for (i=pos-1; i>=0; i--) {
			if (isPartOfString(s, i)) continue;
			char c = s.charAt(i);
			if ((isCharOperator(c) || c==',' || c=='=' || c=='(' || c=='{') && n==0 && m==0)
				break;
			if (c==')') n++;
			if (c=='}') m++;
			if (c=='(') n--;
			if (c=='{') m--;
		} 
		return s.substring(i+1,pos);
	}
	
	/**
	 * Returns true if the char at position pos is inside a string 
	 */
	private boolean isPartOfString(String s, int pos) {
		return getCharCount(s.substring(0,pos),'"')%2==1;
	}
	
	/**
	 * Returns true if the character is an operator
	 */
	private boolean isCharOperator(char c) {
		for (Operator o : operatorManager.getOperators())
			if (c==o.symbol.charAt(0)) // Not a problem to consider only first char since the second one is 
				return true;
		return false;
	}


	/**
	 * Returns a string formatted to explain expected parameters for the function 
	 */
	public String getArgumentCountExceptionString(Pointer pointer, int argc) throws Exception {
		Function f = functionManager.getFunction(pointer);
		return String.format(Error.EXECUTION_WRONG_ARGUMENT_COUNT_FORMAT, argc, f.name);
	}
	
	
	/**
	 * Serialization
	 * Function pointers cannot be serialized, thus they need to be linked
	 * Only user functions are serialized
	 */
	public void saveState(File file) throws IOException {
		ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(file));
		
		// VARIABLES
		// Count non protected variables
		int n = 0;
		for (Variable v : variableManager.variables)
			if ((v.flag&Variable.TYPE_BUILT_IN)==0)
				n++;
		
		// Write them all
		oos.writeInt(n);
		for (Variable v : variableManager.variables)
			if ((v.flag&Variable.TYPE_BUILT_IN)==0)
				oos.writeObject(v);
		
		// FUNCTIONS
		// Count non native function
		n = 0;
		for (Function f : functionManager.functions)
			if ((f.flag&Function.TYPE_BUILT_IN)==0)
				n++;
		
		// Write them all
		oos.writeInt(n);
		for (Function f : functionManager.functions)
			if ((f.flag&Function.TYPE_BUILT_IN)==0)
				oos.writeObject(f);
		
		oos.close();
	}


	public void loadState(File file) throws Exception {
		ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
		
		// Load variables
		int n = ois.readInt();
		for (int i=0; i<n; i++) {
			Variable v = (Variable) ois.readObject();
			variableManager.add(v.name, v.value, v.flag);
		}
			
		// Get and link non native functions
		n = ois.readInt();
		for (int i=0; i<n; i++) {
			Function f = (Function) ois.readObject();
			functionManager.add(f.name, f.target, f.expression, f.flag);
		}
		ois.close();
	}

}

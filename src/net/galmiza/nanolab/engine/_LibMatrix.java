package net.galmiza.nanolab.engine;


import java.math.BigDecimal;

import net.galmiza.nanolab.engine.Function.Pointer;
import Jama.Matrix;

class _LibMatrix extends _LibTools {

	
	/**
	 * Matrix
	 */
	static Pointer zeros() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					Node n1 = node.getChild(0);
					Node n2 = node.getChild(1);
					
					if (n1.isInteger() && n2.isInteger()) {
						node.setList();
						Node row = new Node().setList();
						for (int i=0; i<n2.getInteger().intValue(); i++)	row.addChild(new Node(0.0));
						for (int i=0; i<n1.getInteger().intValue(); i++)	node.addChild(row.copy());
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
			}
		};
	}
	static Pointer ones() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				zeros().run(node); // use zeros() to have a matrix full of zeros
				for (Node n1 : node.getChildren()) 
					for (Node n2 : n1.getChildren())
						n2.setInteger(1);
				return node;
			}
		};
	}
	static Pointer eye() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					if (node.getChild(0).isInteger()) {
						zeros().run(node.addChild(node.getChild(0).copy())); // use zeros() to have a matrix full of zeros
						for (int i=0; i<node.getChildrenCount(); i++)
							node.getChild(i).getChild(i).setInteger(1);
						return node;
					}
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Node size(Node node, Node tmp) {
		if (node.isList()) {
			tmp.addChild(new Node(node.getChildrenCount()));
			return size(node.getChild(0), tmp);
		}
		return null; //exit
	}
	static Pointer inv() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node tmp = node.copy();
					parser.getFunctionPointer("isSquare").run(tmp); // check mat is square
					if (tmp.getBoolean()) {
						double[][] m = getValuesFromNode(node.getChild(0));
					    Matrix M = new Matrix(m);
					    M = M.inverse();
					    return node.copy(getNodeFromValues(M.getArray()));
					} else
						throw new Exception(Error.EXECUTION_DIMENSION_MISMATCH);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer det() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node tmp = node.copy();
					parser.getFunctionPointer("isSquare").run(tmp); // check mat is square
					if (tmp.getBoolean()) {
						double[][] m = getValuesFromNode(node.getChild(0));
					    Matrix M = new Matrix(m);
					    return node.setFloat(new BigDecimal(M.det()));
					} else
						throw new Exception(Error.EXECUTION_DIMENSION_MISMATCH);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
}

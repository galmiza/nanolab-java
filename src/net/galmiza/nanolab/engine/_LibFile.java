package net.galmiza.nanolab.engine;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import net.galmiza.nanolab.engine.Function.Pointer;

class _LibFile extends _LibTools {

	static Pointer ls() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = parser.runNode(node.getChild(0)); 
					if (child.isString()) {
						File path = new File(child.getString());
						node.setList();
						for (File f : path.listFiles())
							node.addChild(new Node(f.getName()));
						return node;
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer fileRead() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = parser.runNode(node.getChild(0)); 
					if (child.isString())	return node.setString(read(new File(child.getString())));
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer fileDelete() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = parser.runNode(node.getChild(0)); 
					if (child.isString())	return node.setBoolean(new File(child.getString()).delete());
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer fileExists() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node child = parser.runNode(node.getChild(0)); 
					if (child.isString())	return node.setBoolean(new File(child.getString()).exists());
					else					throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	static Pointer fileWrite() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==3) {
					Node file = node.getChild(0);
					Node data = node.getChild(1);
					Node append = node.getChild(2);

					if (file.isString() && data.isString() && append.isBoolean())
						write(new File(file.getString()), data.getString(), append.getBoolean());
					else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
					return node.setVoid();
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 3));
			}
		};
	}
	
	
	
	
	
	/**
	 * Read / Write functions
	 */
	static String read(File file) throws Exception {
		int l = (int) file.length();
		byte[] b = new byte[l];
		FileInputStream fis = new FileInputStream(file);
		fis.read(b, 0, l);
		fis.close();
		return new String(b, "UTF-8");
	}
	
	private static void write(File file, String data, boolean append) throws Exception {
		if (file.exists() == false) {
			file.createNewFile();
		}
		if (file.canWrite()) {
			FileOutputStream fos = new FileOutputStream(file, append);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bos.write(data.getBytes("UTF-8"));
			bos.flush();
			bos.close();
		}
	}
}

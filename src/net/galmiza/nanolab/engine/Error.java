package net.galmiza.nanolab.engine;

/**
 * Gathers all error messages
 */
public class Error {

	// Error strings are encrypted to make harder the understanding of the code by a hacker
	public static final String PARSING_MISSING_CLOSING_PARENTHESIS = "Missing closing parenthesis";
	public static final String PARSING_MISSING_CLOSING_BRACKET = "Missing closing bracket";
	public static final String PARSING_MISSING_CLOSING_BRACE = "Missing closing brace";
	public static final String PARSING_MISSING_CLOSING_QUOTE = "Missing closing quote";
	public static final String PARSING_INVALID_TRAILING_CHAR = "Invalid trailing characters";
	public static final String PARSING_INVALID_EXPRESSION = "Invalid expression";

	public static final String EXECUTION_UNKNOWN_KEY = "Unknown key: %1$s";
	public static final String EXECUTION_DIMENSION_MISMATCH = "Dimension mismatch";
	public static final String EXECUTION_INTERNAL_ERROR = "Internal error";
	public static final String EXECUTION_UNDEFINED_VARIABLE = "Undefined variable";
	public static final String EXECUTION_UNDEFINED_FUNCTION = "Undefined function";
	public static final String EXECUTION_WRONG_ARGUMENT_TYPE = "Wrong argument type";
	public static final String EXECUTION_VARIABLE_EXPECTED = "Variable expected";
	public static final String EXECUTION_WRONG_ARGUMENT_COUNT = "Wrong argument count";
	public static final String EXECUTION_WRONG_ARGUMENT_COUNT_FORMAT = "%1$d argument(s) expected for function: %2$s";
	public static final String EXECUTION_WRONG_MIN_ARGUMENT_COUNT = "At least %1$d argument(s) are expected for function: %2$s";

	public static final String VARIABLES_RESERVED_I_COMPLEX = "I is reserved for complex numbers";
	public static final String VARIABLES_PROTECTED_BUILT_IN = "Modifying built-in variables is not allowed";
	public static final String FUNCTIONS_PROTECTED_BUILT_IN = "Modifying built-in functions is not allowed";
	
	

	
}
